﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using DevEngine3;
using DevEngine3.Systems;
using DevEngine3.Containers;
using System.Reflection;

namespace DevEngine3Editor
{
    public partial class EditorWindow : Form
    {
        #region Fields

        private ProjectContainer project;

        #endregion

        #region Properties
        #endregion

        #region Events

        private event Action OnGameStart;

        #endregion

        #region Constructors

        public EditorWindow(ProjectContainer project)
        {
            InitializeComponent();

            this.project = project;

            OnGameStart += EditorWindow_OnGameStart;
        }

        #endregion

        #region Methods

        private void EditorWindow_Load(object sender, EventArgs e)
        {
            Thread thread = new Thread(CreateGame);
            thread.Start();

            LoadAllComponents();
        }

        private void EditorWindow_OnGameStart()
        {
            Camera camera = new Camera("camera");

            Entity entity = new Entity("test", SceneManager.CurrentScene);
            entity.AddComponent<Renderer>();

            ComponentControl control = new ComponentControl(entity.Transform, flowLayoutPanel1);
            flowLayoutPanel1.Controls.Add(control);
        }

        private void CreateGame()
        {
            Invoke((Action)SpawnGame);
        }

        private void SpawnGame()
        {
            string[] files = Directory.GetFiles(project.ProjectPath, "GameInfo.xml", SearchOption.AllDirectories);

            if (files.Length == 0)
            {
                throw new Exception("Missing GameInfo.xml");
            }

            GameInfoContainer gameInfo = Assets.Xml.Read<GameInfoContainer>(files[0]);
            gameInfo.ContentPath = Path.Combine(project.SolutionPath, gameInfo.ContentPath);

            using (DGame game = new DGame(gameInfo))
            {
                game.Start(new SceneManagers.EditorSceneManager(panel3, ref OnGameStart));
            }
        }

        private void EditorWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void LoadAllComponents()
        {
            Type componentType = typeof(Component);

            List<Type> validTypes = new List<Type>();

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    if (componentType.IsAssignableFrom(type))
                    {
                        validTypes.Add(type);
                    }
                }
            }

            foreach (Type type in validTypes)
            {
                comboBox1.Items.Add(type.Name);
            }
        }

        #endregion
    }
}
