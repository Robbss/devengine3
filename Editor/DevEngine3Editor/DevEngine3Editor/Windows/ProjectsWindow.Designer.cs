﻿namespace DevEngine3Editor
{
    partial class ProjectsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectsWindow));
            this.panel1 = new System.Windows.Forms.Panel();
            this.textButton2 = new DevEngine3Editor.TextButton();
            this.textButton1 = new DevEngine3Editor.TextButton();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.progressLine1 = new DevEngine3Editor.ProgressLine();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.textButton2);
            this.panel1.Controls.Add(this.textButton1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1344, 121);
            this.panel1.TabIndex = 0;
            // 
            // textButton2
            // 
            this.textButton2.ActiveColor = System.Drawing.Color.DarkCyan;
            this.textButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.textButton2.DefaultColor = System.Drawing.Color.Black;
            this.textButton2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textButton2.Image = ((System.Drawing.Image)(resources.GetObject("textButton2.Image")));
            this.textButton2.Label = "Open";
            this.textButton2.Location = new System.Drawing.Point(1078, 27);
            this.textButton2.Margin = new System.Windows.Forms.Padding(8, 10, 8, 10);
            this.textButton2.Name = "textButton2";
            this.textButton2.Size = new System.Drawing.Size(240, 73);
            this.textButton2.TabIndex = 2;
            this.textButton2.Click += new System.EventHandler(this.textButton2_Click);
            // 
            // textButton1
            // 
            this.textButton1.ActiveColor = System.Drawing.Color.DarkCyan;
            this.textButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.textButton1.DefaultColor = System.Drawing.Color.Black;
            this.textButton1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textButton1.Image = ((System.Drawing.Image)(resources.GetObject("textButton1.Image")));
            this.textButton1.Label = "New";
            this.textButton1.Location = new System.Drawing.Point(830, 27);
            this.textButton1.Margin = new System.Windows.Forms.Padding(8, 10, 8, 10);
            this.textButton1.Name = "textButton1";
            this.textButton1.Size = new System.Drawing.Size(232, 73);
            this.textButton1.TabIndex = 1;
            this.textButton1.Click += new System.EventHandler(this.textButton1_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 133);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(6);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1344, 569);
            this.flowLayoutPanel1.TabIndex = 1;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // progressLine1
            // 
            this.progressLine1.Color = System.Drawing.Color.DarkCyan;
            this.progressLine1.Location = new System.Drawing.Point(0, 117);
            this.progressLine1.Margin = new System.Windows.Forms.Padding(12);
            this.progressLine1.Name = "progressLine1";
            this.progressLine1.Progress = 0;
            this.progressLine1.Size = new System.Drawing.Size(1344, 4);
            this.progressLine1.TabIndex = 2;
            this.progressLine1.Total = 1000;
            // 
            // ProjectsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1344, 725);
            this.Controls.Add(this.progressLine1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProjectsWindow";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DevEngine3 - Editor";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ProjectsWindow_FormClosed);
            this.Load += new System.EventHandler(this.ProjectsWindow_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private TextButton textButton1;
        private TextButton textButton2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private ProgressLine progressLine1;
    }
}