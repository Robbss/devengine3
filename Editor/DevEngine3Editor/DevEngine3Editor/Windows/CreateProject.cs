﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace DevEngine3Editor
{
    public partial class CreateProject : Form
    {
        #region Fields

        private string path;

        #endregion

        #region Properties

        public string Path
        {
            get
            {
                return path;
            }
            set
            {
                path = value;
            }
        }

        #endregion

        #region Constructors

        public CreateProject()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.None;
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (ProjectHelper.CreateProject(textBox2.Text, textBox1.Text, int.Parse(textBox3.Text), int.Parse(textBox4.Text)))
            {
                Path = System.IO.Path.Combine(textBox2.Text, textBox1.Text);

                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                DialogResult = DialogResult.No;
                Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = dialog.SelectedPath;
            }
        }

        #endregion
    }
}
