﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace DevEngine3Editor
{
    public partial class ProjectsWindow : Form
    {
        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Constructors

        public ProjectsWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        private void ProjectsWindow_Load(object sender, EventArgs e)
        {
            LoadRecentProjects();
            progressLine1.AddProgress(1000);
        }

        private void LoadRecentProjects()
        {
            List<ProjectContainer> projects = new List<ProjectContainer>();

            foreach (string rawPath in FileHelper.GetRecentProjects())
            {
                projects.Add(ProjectHelper.LoadProject(rawPath));
            }

            ProjectContainer[] sortedProjects = projects.OrderBy(o => o.LastOpened).ToArray();
            Array.Reverse(sortedProjects);

            foreach (ProjectContainer project in sortedProjects)
            {
                TextButtonDual instance = new TextButtonDual();

                instance.Label = project.Name;
                instance.Sublabel = project.ProjectPath;

                instance.Width = flowLayoutPanel1.Width - 150;
                instance.Height = 40;

                instance.ActiveColor = Color.DarkCyan;
                instance.Font = new Font("Segoe UI", 10);

                instance.Cursor = Cursors.Hand;
                instance.Tag = project;

                instance.Click += Instance_Click;

                flowLayoutPanel1.Controls.Add(instance);
            }
        }

        private void Instance_Click(object sender, EventArgs e)
        {
            ProjectContainer project = ((Control)sender).Tag as ProjectContainer;

            EditorWindow window = new EditorWindow(project);
            window.Show();

            Close();
        }

        private void textButton1_Click(object sender, EventArgs e)
        {
            CreateProject window = new CreateProject();

            if (window.ShowDialog() == DialogResult.OK)
            {
                FileHelper.AddToRecentProjects(window.Path);
            }
        }

        private void textButton2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.Filter = "DevEngine3 Project|*.project";
            dialog.Title = "Open DevEngine3 Project..";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                ProjectContainer container = ProjectHelper.LoadProject(Path.GetDirectoryName(dialog.FileName));

                EditorWindow window = new EditorWindow(container);
                window.Show();

                Close();
            }
        }

        private void ProjectsWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (Application.OpenForms.Count == 0)
            {
                Application.Exit();
            }
        }

        #endregion
    }
}
