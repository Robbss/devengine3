﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevEngine3Editor
{
    public interface IValue
    {
        object Value { get; }
        event Action<object> OnValueChanged;
    }
}
