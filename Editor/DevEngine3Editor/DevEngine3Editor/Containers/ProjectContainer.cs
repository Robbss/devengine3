﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace DevEngine3Editor
{
    public class ProjectContainer
    {
        #region Fields

        private string name;

        private string projectPath;
        private string solutionPath;
        private string buildPath;

        private DateTime lastOpened;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string SolutionPath
        {
            get
            {
                return solutionPath;
            }
            set
            {
                solutionPath = value;
            }
        }

        [XmlIgnore]
        public string ProjectPath
        {
            get
            {
                return projectPath;
            }
            set
            {
                projectPath = value;
            }
        }

        public string BuildPath
        {
            get
            {
                return buildPath;
            }
            set
            {
                buildPath = value;
            }
        }

        public DateTime LastOpened
        {
            get
            {
                return lastOpened;
            }
            set
            {
                lastOpened = value;
            }
        }

        #endregion

        #region Constructors

        public ProjectContainer()
        {
            name = "DevEngine3Editor-Project";
        }

        public ProjectContainer(string name, string projectPath)
        {
            this.name = name;
            this.lastOpened = DateTime.Now;

            this.projectPath = projectPath;
            this.solutionPath = Path.Combine(projectPath, "Solution");
            this.buildPath = Path.Combine(projectPath, "Build");
        }

        #endregion

        #region Methods
        #endregion
    }
}
