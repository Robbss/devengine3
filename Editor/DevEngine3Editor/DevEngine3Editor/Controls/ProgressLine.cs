﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevEngine3Editor
{
    public partial class ProgressLine : UserControl
    {
        #region Fields

        private int progress = 0;
        private int total = 100;

        private Color color = Color.Red;
        private Brush brush = new SolidBrush(Color.Red);

        #endregion

        #region Properties

        public int Progress
        {
            get
            {
                return progress;
            }
            set
            {
                int previousProgress = progress;
                progress = value;

                if (progress > total)
                {
                    progress = total;
                }

                if (previousProgress != progress)
                {
                    Invalidate();
                }
            }
        }

        public int Total
        {
            get
            {
                return total;
            }
            set
            {
                total = value;
                Invalidate();
            }
        }

        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
                brush = new SolidBrush(value);

                Invalidate();
            }
        }

        #endregion

        #region Constructors

        public ProgressLine()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        protected override void OnPaint(PaintEventArgs e)
        {
            float percentage = (float)progress / (float)total;
            e.Graphics.FillRectangle(brush, new Rectangle(Point.Empty, new Size((int)(Size.Width * percentage), Size.Height)));
        }

        public void AddProgress(int amount)
        {
            Progress += amount;
        }

        #endregion
    }
}
