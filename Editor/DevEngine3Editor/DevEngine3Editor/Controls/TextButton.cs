﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace DevEngine3Editor
{
    public partial class TextButton : UserControl
    {
        #region Fields

        private string label;
        private Image image;

        private bool active = false;

        private Color defaultColor = Color.Black;
        private Color activeColor = Color.Blue;

        private Brush defaultBrush;
        private Brush activeBrush;

        #endregion

        #region Properties

        public Image Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
                Invalidate();
            }
        }

        public string Label
        {
            get
            {
                return label;
            }
            set
            {
                label = value;
                Invalidate();
            }
        }

        public Color DefaultColor
        {
            get
            {
                return defaultColor;
            }
            set
            {
                defaultColor = value;
                Invalidate();
            }
        }

        public Color ActiveColor
        {
            get
            {
                return activeColor;
            }
            set
            {
                activeColor = value;
                Invalidate();
            }
        }

        #endregion

        #region Constructors

        public TextButton()
        {
            InitializeComponent();

            defaultBrush = new SolidBrush(DefaultColor);
            activeBrush = new SolidBrush(ActiveColor);
        }

        #endregion

        #region Methods

        protected override void OnInvalidated(InvalidateEventArgs e)
        {
            defaultBrush = new SolidBrush(DefaultColor);
            activeBrush = new SolidBrush(ActiveColor);

            base.OnInvalidated(e);
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            active = true;
            Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            active = false;
            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (image != null)
            {
                ImageAttributes attributes = new ImageAttributes();

                ColorMap colorMap = new ColorMap();

                colorMap.OldColor = Color.Black;
                colorMap.NewColor = active ? activeColor : defaultColor;

                attributes.SetRemapTable(new ColorMap[] { colorMap });

                e.Graphics.DrawImage(image, new Rectangle(0, 0, Size.Height, Size.Height), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, attributes);
            }

            e.Graphics.DrawString(label, Font, active ? activeBrush : defaultBrush, new Point(Size.Height, Size.Height / 2 - Font.Height / 2));
        }

        #endregion
    }
}
