﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevEngine3Editor
{
    public partial class ComponentSliderControl : UserControl, IValue
    {
        #region Fields

        private bool isFloat = false;
        private object value = null;

        #endregion

        #region Properties

        public object Value
        {
            get
            {
                return value;
            }
        }

        #endregion

        #region Events

        public event Action<object> OnValueChanged;

        #endregion

        #region Constructors

        public ComponentSliderControl(object current, object min, object max)
        {
            InitializeComponent();

            if (min is float || max is float)
            {
                float minf = (float)min;
                float maxf = (float)max;

                minf *= 100;
                maxf *= 100;

                trackBar1.Minimum = (int)Math.Round(minf);
                trackBar1.Maximum = (int)Math.Round(maxf);

                isFloat = true;
            }
            else
            {
                trackBar1.Minimum = (int)min;
                trackBar1.Maximum = (int)max;
            }

            trackBar1.ValueChanged += TrackBar1_ValueChanged;
            trackBar1.TickFrequency = 1000;

            if (isFloat)
            {
                trackBar1.Value = (int)Math.Round((float)current * 100.0f);
            }
            else
            {
                trackBar1.Value = (int)current;
            }
        }

        #endregion

        #region Methods

        private void TrackBar1_ValueChanged(object sender, EventArgs e)
        {
            if (isFloat)
            {
                value = ((float)trackBar1.Value / 100.0f);
            }
            else
            {
                value = trackBar1.Value;
            }

            OnValueChanged?.Invoke(value);

            label1.Text = value.ToString();
        }

        #endregion
    }
}
