﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevEngine3.Containers;
using Microsoft.Xna.Framework;
using DevEngine3;
using System.Reflection;
using DevEngine3.Systems;

namespace DevEngine3Editor
{
    public partial class ComponentControl : UserControl
    {
        #region Fields

        private Component component;

        #endregion

        #region Properties
        #endregion

        #region Constructors

        public ComponentControl(Component component, Panel parent)
        {
            InitializeComponent();

            this.component = component;

            this.checkBoxEnabled.Text = component.GetType().Name;
            this.checkBoxEnabled.Checked = component.Enabled;

            this.Width = parent.Width - 6;

            if (component is IEditorView)
            {
                List<EditorViewItem[]> items = (component as IEditorView).GetEditorItems();

                foreach (EditorViewItem[] row in items)
                {
                    Height += 25;

                    Label title = new Label();

                    string path = row[0].ValuePath;
                    string name = path;

                    if (path.Contains('.'))
                    {
                        name = path.Substring(0, path.LastIndexOf('.'));

                        if (name.Contains('.'))
                        {
                            name = name.Substring(path.LastIndexOf('.'));
                        }
                    }

                    title.Text = name;
                    title.Location = new System.Drawing.Point(0, Height - title.Height);
                    title.TextAlign = ContentAlignment.MiddleLeft;
                    title.Width = Width / 5;
                    title.AutoSize = false;

                    List<Control> controls = new List<Control>() { title };

                    foreach (EditorViewItem item in row)
                    {
                        IValue iValue = null;

                        switch (item.DisplayMode)
                        {
                            case EditorDisplayMode.TextBox:
                                break;

                            case EditorDisplayMode.Slider:
                                iValue = new ComponentSliderControl(item.Parameters[2], item.Parameters[0], item.Parameters[1]) { Width = (Width - title.Width) / 2 };
                                break;
                        }

                        iValue.OnValueChanged += (o) =>
                        {
                            HandleValueChanged(item, o);
                        };

                        Control control = iValue as Control;

                        control.Location = new System.Drawing.Point(controls.Last().Right, Height - title.Height);

                        controls.Add(control);
                    }

                    foreach (Control control in controls)
                    {
                        Controls.Add(control);
                    }
                }
            }
            else
            {
                throw new Exception("Can't be shown in editor");
            }
        }

        #endregion

        #region Methods

        private void HandleValueChanged(EditorViewItem item, object value)
        {

        }

        #endregion
    }
}
