﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevEngine3Editor
{
    public partial class TextButtonDual : TextButton
    {
        #region Fields

        private string sublabel;
        private Color subColor = Color.Gray;
        private Brush subBrush;

        #endregion

        #region Properties

        public string Sublabel
        {
            get
            {
                return sublabel;
            }
            set
            {
                sublabel = value;
                Invalidate();
            }
        }

        public Color SubColor
        {
            get
            {
                return subColor;
            }
            set
            {
                subColor = value;
                Invalidate();
            }
        }

        #endregion

        #region Constructors

        public TextButtonDual()
        {
            InitializeComponent();
            subBrush = new SolidBrush(subColor);
        }

        #endregion

        #region Methods

        protected override void OnInvalidated(InvalidateEventArgs e)
        {
            base.OnInvalidated(e);
            subBrush = new SolidBrush(subColor);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.DrawString(sublabel, Font, subBrush, new Point(Size.Height, Size.Height / 2 + Font.Height / 2));
        }

        #endregion
    }
}
