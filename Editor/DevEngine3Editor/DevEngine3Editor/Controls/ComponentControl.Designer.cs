﻿namespace DevEngine3Editor
{
    partial class ComponentControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBoxEnabled = new System.Windows.Forms.CheckBox();
            this.hRule1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // checkBoxEnabled
            // 
            this.checkBoxEnabled.Location = new System.Drawing.Point(3, 0);
            this.checkBoxEnabled.Name = "checkBoxEnabled";
            this.checkBoxEnabled.Size = new System.Drawing.Size(360, 36);
            this.checkBoxEnabled.TabIndex = 0;
            this.checkBoxEnabled.Text = "ComponentName";
            this.checkBoxEnabled.UseVisualStyleBackColor = true;
            // 
            // hRule1
            // 
            this.hRule1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hRule1.Location = new System.Drawing.Point(0, 34);
            this.hRule1.Name = "hRule1";
            this.hRule1.Size = new System.Drawing.Size(366, 2);
            this.hRule1.TabIndex = 1;
            // 
            // ComponentControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.Controls.Add(this.hRule1);
            this.Controls.Add(this.checkBoxEnabled);
            this.Name = "ComponentControl";
            this.Size = new System.Drawing.Size(366, 41);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBoxEnabled;
        private System.Windows.Forms.Label hRule1;
    }
}
