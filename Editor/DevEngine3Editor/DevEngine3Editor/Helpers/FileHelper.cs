﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DevEngine3Editor
{
    public static class FileHelper
    {
        #region Fields

        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Methods

        public static string[] GetRecentProjects()
        {
            List<string> strings = new List<string>();

            if (!File.Exists("Config/recent.txt"))
            {
                return strings.ToArray();
            }

            string[] lines = File.ReadAllLines("Config/recent.txt");

            foreach (string item in lines)
            {
                if (Directory.Exists(item))
                {
                    string[] files = Directory.GetFiles(item, "*.project", SearchOption.TopDirectoryOnly);

                    if (files.Length != 0)
                    {
                        strings.Add(item);
                    }
                }
            }

            return strings.ToArray();
        }

        public static void AddToRecentProjects(string item)
        {
            List<string> projects = new List<string>();

            using (StreamReader reader = new StreamReader("Config/recent.txt"))
            {
                projects.Add(reader.ReadLine());
            }

            if (!projects.Contains(item))
            {
                using (StreamWriter writer = new StreamWriter("Config/recent.txt", true))
                {
                    writer.WriteLine(item);
                }
            }
        }

        #endregion
    }
}
