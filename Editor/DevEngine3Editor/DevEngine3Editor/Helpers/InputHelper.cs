﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevEngine3Editor
{
    public static class InputHelper
    {
        #region Fields

        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Methods

        public static bool IsValid(Type type, string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return true;
            }

            if (type == typeof(byte))
            {
                byte result;
                return byte.TryParse(input, out result);
            }

            if (type == typeof(int))
            {
                int result;
                return int.TryParse(input, out result);
            }

            if (type == typeof(float))
            {
                float result;
                return float.TryParse(input, out result);
            }

            if (type == typeof(double))
            {
                double result;
                return double.TryParse(input, out result);
            }

            if (type == typeof(string))
            {
                return true;
            }

            if (type == typeof(Single))
            {
                Single result;
                return Single.TryParse(input, out result);
            }

            if (type == typeof(uint))
            {
                uint result;
                return uint.TryParse(input, out result);
            }

            return false;
        }

        public static object Convert(Type type, string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return null;
            }

            if (type == typeof(int))
            {
                int result;
                int.TryParse(input, out result);

                return result;
            }

            if (type == typeof(float))
            {
                float result;
                float.TryParse(input, out result);

                return result;
            }

            if (type == typeof(double))
            {
                double result;
                double.TryParse(input, out result);

                return result;
            }

            if (type == typeof(string))
            {
                return input;
            }

            if (type == typeof(Single))
            {
                Single result;
                Single.TryParse(input, out result);

                return result;
            }

            if (type == typeof(byte))
            {
                byte result;
                byte.TryParse(input, out result);

                return result;
            }

            if (type == typeof(uint))
            {
                uint result;
                uint.TryParse(input, out result);

                return result;
            }

            return null;
        }

        #endregion
    }
}
