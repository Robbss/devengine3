﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using DevEngine3;
using DevEngine3.Systems;
using DevEngine3.Globals;
using DevEngine3.Containers;

namespace DevEngine3Editor
{
    public static class ProjectHelper
    {
        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Methods

        public static bool CreateProject(string path, string name, int vwidth, int vheight)
        {
            string actualPath = Path.Combine(path, name);

            if (!Directory.Exists(actualPath))
            {
                Directory.CreateDirectory(actualPath);
            }

            string solutionPath = Path.Combine(actualPath, "Solution");

            if (Directory.Exists(solutionPath))
            {
                Directory.Delete(solutionPath, true);
            }

            ZipFile.ExtractToDirectory("Content/ProjectPrefabs/Default.zip", solutionPath);

            GameInfoContainer container = new GameInfoContainer(name, "Content", true, false, new Size(vwidth, vheight), new Size(vwidth, vheight), "DevEngine3.SceneManagers.DefaultSceneManager", "Scenes/DefaultScene.xml");
            Assets.Xml.Write(container, Path.Combine(solutionPath, "GameInfo.xml"));

            ProjectContainer projectContainer = new ProjectContainer(name, actualPath);
            Assets.Xml.Write(projectContainer, Path.Combine(actualPath, $"{name}.project"));

            return true;
        }

        public static ProjectContainer LoadProject(string path)
        {
            if (!Directory.Exists(path))
            {
                return null;
            }

            string[] projectFiles = Directory.GetFiles(path, "*.project", SearchOption.TopDirectoryOnly);

            if (projectFiles.Length == 0)
            {
                return null;
            }

            ProjectContainer container = Assets.Xml.Read<ProjectContainer>(projectFiles[0]);
            container.ProjectPath = path;

            container.LastOpened = DateTime.Now;

            FileHelper.AddToRecentProjects(container.ProjectPath);
            Assets.Xml.Write(container, projectFiles[0]);

            return container;
        }

        #endregion
    }
}
