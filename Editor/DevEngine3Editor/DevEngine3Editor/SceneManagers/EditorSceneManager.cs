﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevEngine3;
using DevEngine3.Interfaces;
using DevEngine3.Globals;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DevEngine3Editor.SceneManagers
{
    public class EditorSceneManager : ISceneManager
    {
        #region Fields

        private Scene scene;
        private Control surface;

        private Action callback;

        #endregion

        #region Properties

        public Scene Current
        {
            get
            {
                return scene;
            }
        }

        #endregion

        #region Constructors

        public EditorSceneManager(Control surface, ref Action callback)
        {
            this.surface = surface;
            this.callback = callback;
        }

        #endregion

        #region Methods

        public void ChangeScene(Scene scene)
        {
            this.scene = scene;
        }

        public bool Start(ref GameWrapper gameWrapper, ref GraphicsDeviceManager graphicsDeviceManager)
        {
            graphicsDeviceManager.PreparingDeviceSettings += GraphicsDeviceManager_PreparingDeviceSettings;

            graphicsDeviceManager.PreferredBackBufferWidth = surface.Width;
            graphicsDeviceManager.PreferredBackBufferHeight = surface.Height;
            graphicsDeviceManager.IsFullScreen = false;

            Mouse.WindowHandle = surface.FindForm().Handle;                       

            return true;
        }

        private void GraphicsDeviceManager_PreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            e.GraphicsDeviceInformation.PresentationParameters.DeviceWindowHandle = surface.Handle;
        }

        public void Setup()
        {
            scene.Setup();
            callback?.Invoke();
        }

        public void Update(GameTime gameTime)
        {
            scene.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            scene.Draw(spriteBatch);
        }

        #endregion
    }
}
