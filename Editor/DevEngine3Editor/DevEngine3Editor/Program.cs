﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevEngine3.Systems;

namespace DevEngine3Editor
{
    static class Program
    {
        static Program()
        {
            AssemblyResolver.PrepareAssemblyResolver();
        }

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            ProjectsWindow window = new ProjectsWindow();
            window.Show();

            Application.Run();
        }
    }
}
