﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using DevEngine3.Systems;

namespace DevEngine3
{
    public class Shader : Component
    {
        #region Fields

        private List<string> techniques;
        private Effect effect;

        #endregion

        #region Properties

        [SerializeExtra]
        public List<string> Techniques
        {
            get
            {
                return techniques;
            }
            set
            {
                techniques = value;
            }
        }

        [SerializeExtraFrom("SetEffect", "EffectID")]
        public Effect Effect
        {
            get
            {
                return effect;
            }
            set
            {
                effect = value;
            }
        }

        private string EffectID
        {
            get
            {
                return Assets.GetAssetName(effect);
            }
        }

        #endregion

        #region Constructors

        public Shader()
        {
            techniques = new List<string>();
        }

        #endregion

        #region Methods

        public override void Setup()
        {
            Entity.RenderMode = RenderMode.Shader;
        }

        public void SetParameter(string name, float value)
        {
            effect.Parameters[name].SetValue(value);
        }

        public void SetParameter(string name, float[] value)
        {
            effect.Parameters[name].SetValue(value);
        }

        public void SetParameter(string name, Texture value)
        {
            effect.Parameters[name].SetValue(value);
        }

        public void SetParameter(string name, int value)
        {
            effect.Parameters[name].SetValue(value);
        }

        public void SetParameter(string name, bool value)
        {
            effect.Parameters[name].SetValue(value);
        }

        public T GetParameter<T>(string name)
        {
            if (typeof(T) == typeof(float))
            {
                return (T)(object)effect.Parameters[name].GetValueSingle();
            }

            return default(T);
        }

        public void SetEffect(string name)
        {
            effect = Assets.Load<Effect>(name);
        }

        #endregion
    }
}
