﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using DevEngine3.Systems;

namespace DevEngine3
{
    public class Renderer : Component, IDrawable
    {
        #region Fields

        private Texture2D texture;
        private Color color;

        private Vector2 origin;
        private Vector2 offset;

        private float layer;

        #endregion

        #region Properties

        [SerializeExtraFrom("SetTexture", "TextureID")]
        public Texture2D Texture
        {
            get
            {
                return texture;
            }
            set
            {
                texture = value;
            }
        }

        private string TextureID
        {
            get
            {
                return Assets.GetAssetName(texture);
            }
        }

        [SerializeExtra]
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        [SerializeExtra]
        public Vector2 Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
            }
        }

        [SerializeExtra]
        public Vector2 Offset
        {
            get
            {
                return offset;
            }
            set
            {
                offset = value;
            }
        }

        [SerializeExtra]
        public float Layer
        {
            get
            {
                return layer;
            }
            set
            {
                layer = value;
            }
        }

        #endregion

        #region Constructors

        public Renderer() : base()
        {
            this.texture = null;
            this.color = Color.White;

            this.origin = Vector2.Zero;
            this.offset = Vector2.Zero;

            this.layer = 0.5f;
        }

        public Renderer(Texture2D texture) : base()
        {
            this.texture = texture;
            this.color = Color.White;

            this.origin = Vector2.Zero;
            this.offset = Vector2.Zero;

            this.layer = 0.5f;
        }

        #endregion

        #region Methods

        public void Invalidate()
        {
            origin = texture.GetCenter();
        }

        public override void Setup()
        {
            Invalidate();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture.GetTexture(), destinationRectangle: new Rectangle(Transform.Position.ToPoint() + Offset.ToPoint() + (Origin / texture.GetTexture().Bounds.Size.ToVector2() * Transform.Size).ToPoint(), Transform.Size.ToPoint()), sourceRectangle: texture.GetTexture().Bounds, origin: Origin, rotation: Transform.Rotation, effects: SpriteEffects.None, layerDepth: Layer, color: Color);
        }

        public void SetTexture(string name)
        {
            texture = Assets.Load<Texture2D>(name);
        }

        #endregion
    }
}
