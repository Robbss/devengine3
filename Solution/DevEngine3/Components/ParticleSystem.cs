﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DevEngine3.Systems;

namespace DevEngine3
{
    public class ParticleSystem : Component, IDrawable, IUpdateable
    {
        #region Fields

        private Particle[] particles;
        private Queue<Particle> freeParticles;

        private int maxParticles = 500;

        private float layer = 0;

        private float spawnDelay = 0f;
        private float spawnDelayElapsed = 0f;

        private float minSpawnDelay = 0.01f;
        private float maxSpawnDelay = 0.1f;

        private float minSpeed = 10f;
        private float maxSpeed = 10f;

        private float minAcceleration = 0f;
        private float maxAcceleration = 0f;

        private float minRotationSpeed = 0f;
        private float maxRotationSpeed = 0f;

        private float minLifetime = 1f;
        private float maxLifetime = 5f;

        private float minScale = 1f;
        private float maxScale = 5f;

        private Texture2D particleTexture;

        #endregion

        #region Properties

        [SerializeExtra]
        public float MinSpawnRate
        {
            get
            {
                return minSpawnDelay;
            }
            set
            {
                minSpawnDelay = value;
            }
        }

        [SerializeExtra]
        public float MaxSpawnDelay
        {
            get
            {
                return maxSpawnDelay;
            }

            set
            {
                maxSpawnDelay = value;
            }
        }

        [SerializeExtra]
        public float MinSpeed
        {
            get
            {
                return minSpeed;
            }
            set
            {
                minSpeed = value;
            }
        }

        [SerializeExtra]
        public float MaxSpeed
        {
            get
            {
                return maxSpeed;
            }
            set
            {
                maxSpeed = value;
            }
        }

        [SerializeExtra]
        public float MinAcceleration
        {
            get
            {
                return minAcceleration;
            }
            set
            {
                minAcceleration = value;
            }
        }

        [SerializeExtra]
        public float MaxAcceleration
        {
            get
            {
                return maxAcceleration;
            }
            set
            {
                maxAcceleration = value;
            }
        }

        [SerializeExtra]
        public float MinRotationSpeed
        {
            get
            {
                return minRotationSpeed;
            }
            set
            {
                minRotationSpeed = value;
            }
        }

        [SerializeExtra]
        public float MaxRotationSpeed
        {
            get
            {
                return maxRotationSpeed;
            }
            set
            {
                maxRotationSpeed = value;
            }
        }

        [SerializeExtra]
        public float MinLifetime
        {
            get
            {
                return minLifetime;
            }
            set
            {
                minLifetime = value;
            }
        }

        [SerializeExtra]
        public float MaxLifetime
        {
            get
            {
                return maxLifetime;
            }
            set
            {
                maxLifetime = value;
            }
        }

        [SerializeExtra]
        public float MinScale
        {
            get
            {
                return minScale;
            }
            set
            {
                minScale = value;
            }
        }

        [SerializeExtra]
        public float MaxScale
        {
            get
            {
                return maxScale;
            }
            set
            {
                maxScale = value;
            }
        }

        [SerializeExtra]
        public float Layer
        {
            get
            {
                return layer;
            }
            set
            {
                layer = value;
            }
        }

        [SerializeExtraFrom("SetTexture", "ParticleTextureID")]
        public Texture2D ParticleTexture
        {
            get
            {
                return particleTexture;
            }
            set
            {
                particleTexture = value;
            }
        }

        private string ParticleTextureID
        {
            get
            {
                return Assets.GetAssetName(particleTexture);
            }
        }

        #endregion

        #region Constructors

        public ParticleSystem() : base()
        {
            this.freeParticles = new Queue<Particle>();
        }

        public ParticleSystem(int maxParticles) : base()
        {
            this.maxParticles = maxParticles;
            this.freeParticles = new Queue<Particle>();
        }

        #endregion

        #region Methods

        public override void Setup()
        {
            particles = new Particle[maxParticles];

            for (int i = 0; i < maxParticles; i++)
            {
                Particle particle = new Particle(RandomHelper.RandomString("particle"), null);

                particles[i] = particle;
                freeParticles.Enqueue(particle);
            }
        }

        public bool SpawnParticle()
        {
            if (freeParticles.Count == 0)
            {
                return false;
            }

            Particle particle = freeParticles.Dequeue();

            Vector2 direction = RandomHelper.RandomDirection();

            float speed = RandomHelper.Between(minSpeed, maxSpeed);
            float acceleration = RandomHelper.Between(minAcceleration, maxAcceleration);
            float lifetime = RandomHelper.Between(minLifetime, maxLifetime);
            float scale = RandomHelper.Between(minScale, maxScale);
            float rotationSpeed = RandomHelper.Between(minRotationSpeed, maxRotationSpeed);

            particle.Initialize(Transform.Center, speed * direction, acceleration * direction, lifetime, scale, rotationSpeed);

            return true;
        }

        public void Update(GameTime gameTime)
        {
            if (spawnDelayElapsed > spawnDelay && SpawnParticle())
            {
                spawnDelay = RandomHelper.Between(minSpawnDelay, maxSpawnDelay);
                spawnDelayElapsed = 0;
            }

            foreach (Particle particle in particles)
            {
                if (particle.Enabled)
                {
                    particle.Update(gameTime);

                    if (!particle.Enabled)
                    {
                        freeParticles.Enqueue(particle);
                    }
                }
            }

            spawnDelayElapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Particle particle in particles)
            {
                if (!particle.Enabled)
                {
                    continue;
                }

                float normalizedLifetime = particle.LifetimeElapsed / particle.Lifetime;

                float alpha = 4 * normalizedLifetime * (1 - normalizedLifetime);
                Color color = Color.White * alpha;

                Vector2 origin = particleTexture.GetCenter();

                float scale = particle.Transform.Scale.Width * (.75f + .25f * normalizedLifetime);

                spriteBatch.Draw(particleTexture.GetTexture(), new Rectangle((particle.Transform.Position - origin).ToPoint(), (particle.Transform.Size * scale).ToPoint()), null, color, particle.Transform.Rotation, origin, SpriteEffects.None, layer);
            }
        }

        public void SetTexture(string textureid)
        {
            particleTexture = Assets.Load<Texture2D>(textureid);
        }

        #endregion
    }
}
