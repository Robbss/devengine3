﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public class Clickable : Component, IUpdateable
    {
        #region Fields

        private bool hovering = false;

        #endregion

        #region Properties
        #endregion

        #region Events

        public event Action OnMouseEnter;
        public event Action OnMouseLeave;

        public event Action<MouseButtons> OnMouseDown;
        public event Action<MouseButtons> OnMouseUp;

        #endregion

        #region Constructors

        public Clickable() : base()
        {
        }

        #endregion

        #region Methods

        public void Update(GameTime gameTime)
        {
            if (Transform.Bounds.Contains(Camera.Current.ScreenPointToWorld(Input.MousePosition)))
            {
                if (!hovering)
                {
                    OnMouseEnter?.Invoke();
                    hovering = true;
                }

                if (Input.GetMouseButtonDown(MouseButtons.Left))
                {
                    OnMouseDown?.Invoke(MouseButtons.Left);
                }

                if (Input.GetMouseButtonDown(MouseButtons.Right))
                {
                    OnMouseDown?.Invoke(MouseButtons.Right);
                }

                if (Input.GetMouseButtonUp(MouseButtons.Left))
                {
                    OnMouseUp?.Invoke(MouseButtons.Left);
                }

                if (Input.GetMouseButtonUp(MouseButtons.Right))
                {
                    OnMouseUp?.Invoke(MouseButtons.Right);
                }
            }
            else if (hovering)
            {
                OnMouseLeave?.Invoke();
                hovering = false;
            }
        }

        #endregion

    }
}
