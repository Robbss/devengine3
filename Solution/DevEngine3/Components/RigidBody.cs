﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public class RigidBody : Component
    {
        #region Fields

        public Vector2 position
        {
            get
            {
                return Transform.Center;
            }
            set
            {
                Transform.Center = value;
            }
        }

        public Vector2 velocity;

        public float angularVelocity;
        public float torque;

        public float orientation
        {
            get
            {
                return Transform.Rotation;
            }
            set
            {
                Transform.Rotation = value;
                shape.SetOrient(value);
            }
        }

        public Vector2 force;

        public float I;
        public float iI;
        public float m;
        public float im;

        public float staticFriction;
        public float dynamicFriction;
        public float restitution;

        public Shape shape;

        #endregion

        #region Properties
        #endregion

        #region Constructors

        public RigidBody() : this(new Circle(10))
        {
        }

        public RigidBody(Shape shape)
        {
            this.shape = shape;
            this.shape.Body = this;

            velocity = new Vector2();
            angularVelocity = 0;

            torque = 0;
            force = new Vector2();

            staticFriction = 0.5f;
            dynamicFriction = 0.3f;

            restitution = 1f;

            shape.Initialize();
        }

        #endregion

        #region Methods

        public void SetStatic()
        {
            I = 0.0f;
            iI = 0.0f;
            m = 0.0f;
            im = 0.0f;
        }

        public void ApplyForce(Vector2 f)
        {
            force += f;
        }

        public void ApplyImpulse(Vector2 impulse, Vector2 contactVector)
        {
            velocity += im * impulse;
            angularVelocity += iI * contactVector.Cross(impulse);
        }

        #endregion
    }
}
