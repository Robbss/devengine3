﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public class Transform : Component, IEditorView
    {
        #region Fields

        private Vector2 position;
        private Sizef size;
        private float rotation;
        private Sizef scale;

        private Transform parent;

        #endregion

        #region Properties

        [SerializeExtra]
        public Vector2 LocalPosition
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        public Vector2 Position
        {
            get
            {
                if (parent != null)
                {
                    return parent.Position + position;
                }

                return position;
            }
            set
            {
                position = value;
            }
        }

        [SerializeExtra]
        public Sizef Size
        {
            get
            {
                return size;
            }
            set
            {
                size = value;
            }
        }

        [SerializeExtra]
        public float Rotation
        {
            get
            {
                return rotation;
            }
            set
            {
                rotation = value;
            }
        }

        [SerializeExtra]
        public Sizef Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
            }
        }

        [SerializeExtraFrom("SetParentFromName", "Parent.Entity.Name")]
        public Transform Parent
        {
            get
            {
                return parent;
            }
            set
            {
                parent = value;
            }
        }

        public Vector2 Center
        {
            get
            {
                return new Vector2(position.X + size.Width / 2, position.Y + size.Height / 2);
            }
            set
            {
                position = new Vector2(value.X - size.Width / 2, value.Y - size.Height / 2);
            }
        }

        public Rectangle Bounds
        {
            get
            {
                return new Rectangle((int)position.X, (int)position.Y, (int)size.Width, (int)size.Height);
            }
        }

        #endregion

        #region Constructors

        public Transform()
        {
            this.position = new Vector2();
            this.size = new Sizef();
            this.rotation = 0.0f;
            this.scale = Sizef.One;
        }

        public Transform(Vector2 position, Sizef size)
        {
            this.position = position;
            this.size = size;
            this.rotation = 0.0f;
            this.scale = Sizef.One;
        }

        #endregion

        #region Methods

        public virtual void Translate(float x, float y) // ???
        {
            this.position.X += x;
            this.position.Y += y;
        }

        public virtual void SetParent(Entity entity)
        {
            Parent = entity.GetComponent<Transform>();
        }

        public virtual void SetParentFromName(string name)
        {
            Parent = Entity.Find(name).GetComponent<Transform>();
        }

        public List<EditorViewItem[]> GetEditorItems()
        {
            List<EditorViewItem[]> total = new List<EditorViewItem[]>();

            List<EditorViewItem> row0 = new List<EditorViewItem>();

            row0.Add(new EditorViewItem("LocalPosition.X", typeof(float), EditorDisplayMode.Slider, 0.0f, 100.0f, 0.0f));
            row0.Add(new EditorViewItem("LocalPosition.Y", typeof(float), EditorDisplayMode.Slider, 0.0f, 100.0f, 0.0f));

            total.Add(row0.ToArray());

            return total;
        }

        #endregion
    }
}
