﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public static class Vector2Extensions
    {

        public static Vector2 Cross(this Vector2 v, float a)
        {
            return new Vector2(-a * v.Y, a * v.X);
        }

        public static float Cross(this Vector2 a, Vector2 b)
        {
            return a.X * b.Y - a.Y * b.X;
        }

    }
}
