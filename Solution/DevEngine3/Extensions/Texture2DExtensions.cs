﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DevEngine3.Systems;

namespace DevEngine3
{
    public static class Texture2DExtensions
    {
        #region Fields

        private static Texture2D errorTexture;

        #endregion

        #region Properties

        public static Texture2D ErrorTexture
        {
            get
            {
                if (errorTexture == null)
                {
                    errorTexture = Assets.Load<Texture2D>("DevEngine3.Error.png");
                }

                return errorTexture;
            }
        }

        #endregion

        #region Constructors
        #endregion

        #region Methods

        public static Texture2D GetTexture(this Texture2D texture)
        {
            if (texture == null)
            {
                return ErrorTexture;
            }
            else
            {
                return texture;
            }
        }

        public static Vector2 GetCenter(this Texture2D texture)
        {
            Texture2D actual = texture.GetTexture();
            return new Vector2(actual.Width / 2.0f, actual.Height / 2.0f);
        }

        #endregion
    }
}
