﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevEngine3
{
    public class GenericException : Exception
    {
        #region Fields

        private string section;
        private string description;

        #endregion

        #region Properties

        public string Section
        {
            get
            {
                return section;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
        }

        #endregion

        #region Constructors

        public GenericException(string section, string description)
        {
            this.section = section;
            this.description = description;
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            return $"[{section}] {description}";
        }

        #endregion
    }
}
