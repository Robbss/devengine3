﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DevEngine3.Interfaces
{
    public interface IAsset<T>
    {
        T FromStream(Stream stream);
        Stream ToStream();
    }
}
