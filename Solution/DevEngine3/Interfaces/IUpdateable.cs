﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public interface IUpdateable
    {
        void Update(GameTime gameTime);
    }
}
