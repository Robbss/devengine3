﻿using DevEngine3.Systems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevEngine3
{
    public interface IServerInterface
    {
        void OnClientConnected(Client client);
        void OnClientDisconnected(Client client);

        void Tick(int delta);
        void OnRunningSlow();

        void Initialize(Multiplayer.Server server, object[] arguments);
    }
}
