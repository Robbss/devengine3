﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DevEngine3.Interfaces
{
    public interface ISceneManager
    {
        Scene Current { get; }

        bool Start(ref GameWrapper gameWrapper, ref GraphicsDeviceManager graphicsDeviceManager);
        void Setup();
        void Update(GameTime gameTime);
        void Draw(SpriteBatch spriteBatch);

        void ChangeScene(Scene scene);
    }
}
