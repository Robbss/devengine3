﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DevEngine3.Interfaces;
using DevEngine3.Systems;

namespace DevEngine3.Containers
{
    [Serializable, XmlRoot("GameInfo")]
    public class GameInfoContainer : IAsset<GameInfoContainer>
    {
        #region Fields

        private string name;
        private string contentPath;

        private bool mouseVisible;
        private bool fullScreen;

        private Size virtualResolution;
        private Size resolution;

        private string sceneManager;
        private string scene;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string ContentPath
        {
            get
            {
                return contentPath;
            }
            set
            {
                contentPath = value;
            }
        }

        public bool MouseVisible
        {
            get
            {
                return mouseVisible;
            }
            set
            {
                mouseVisible = value;
            }
        }

        public bool FullScreen
        {
            get
            {
                return fullScreen;
            }
            set
            {
                fullScreen = value;
            }
        }

        public Size Resolution
        {
            get
            {
                return resolution;
            }
            set
            {
                resolution = value;
            }
        }

        public string SceneManager
        {
            get
            {
                return sceneManager;
            }
            set
            {
                sceneManager = value;
            }
        }

        public string Scene
        {
            get
            {
                return scene;
            }
            set
            {
                scene = value;
            }
        }

        public Size VirtualResolution
        {
            get
            {
                return virtualResolution;
            }
            set
            {
                virtualResolution = value;
            }
        }

        #endregion

        #region Constructors

        public GameInfoContainer()
        {
            return;
        }

        public GameInfoContainer(string name, string contentPath, bool mouseVisible, bool fullScreen, Size virtualResolution, Size resolution, string sceneManager, string scene)
        {
            this.name = name;
            this.contentPath = contentPath;
            this.mouseVisible = mouseVisible;
            this.fullScreen = fullScreen;
            this.virtualResolution = virtualResolution;
            this.resolution = resolution;
            this.sceneManager = sceneManager;
            this.scene = scene;
        }

        #endregion

        #region Methods

        public GameInfoContainer FromStream(Stream stream)
        {
            return Assets.Xml.Read<GameInfoContainer>(stream);
        }

        public Stream ToStream()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
