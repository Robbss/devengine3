﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3.Containers
{
    public class SceneModifiers
    {
        #region Fields

        private bool clear;
        private Color clearColor;

        #endregion

        #region Properties

        public bool Clear
        {
            get
            {
                return clear;
            }
            set
            {
                clear = value;
            }
        }

        public Color ClearColor
        {
            get
            {
                return clearColor;
            }
            set
            {
                clearColor = value;
            }
        }

        #endregion

        #region Constructors

        public SceneModifiers()
        {
            this.clear = true;
            this.clearColor = Color.LightBlue;
        }

        public SceneModifiers(bool clear)
        {
            this.clear = clear;
            this.clearColor = Color.LightBlue;
        }

        public SceneModifiers(Color clearColor)
        {
            this.clear = true;
            this.clearColor = clearColor;
        }

        #endregion

        #region Methods
        #endregion
    }
}
