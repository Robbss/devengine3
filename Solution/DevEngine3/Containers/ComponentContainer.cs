﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using DevEngine3.Systems;
using DevEngine3.Helpers;

namespace DevEngine3.Containers
{
    public class ComponentContainer
    {
        #region Fields

        private string type;
        private bool enabled;

        private SerializableDictionary<string, object> extras;

        #endregion

        #region Properties

        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
            }
        }

        public SerializableDictionary<string, object> Extras
        {
            get
            {
                return extras;
            }
            set
            {
                extras = value;
            }
        }

        #endregion

        #region Constructors

        public ComponentContainer()
        {
            this.enabled = true;
            this.extras = new SerializableDictionary<string, object>();
        }

        public ComponentContainer(string type)
        {
            this.type = type;
            this.enabled = true;
            this.extras = new SerializableDictionary<string, object>();
        }

        #endregion

        #region Methods

        public Component CreateComponent(Entity entity)
        {
            Component component = Assets.Activator.CreateInstance<Component>(type);

            component.Enabled = enabled;
            ExtrasHelper.SetExtras(component, extras);

            entity.AddComponent(component);

            return component;
        }

        public static ComponentContainer GetContainer(Component component)
        {
            ComponentContainer container = new ComponentContainer();

            container.Type = component.GetType().FullName;
            container.Enabled = component.Enabled;
            container.Extras = ExtrasHelper.GetExtras(component);

            return container;
        }

        #endregion
    }
}
