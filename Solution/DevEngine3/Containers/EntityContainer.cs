﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DevEngine3.Systems;
using DevEngine3.Interfaces;
using DevEngine3.Helpers;

namespace DevEngine3.Containers
{
    [Serializable, XmlRoot("Entity")]
    public class EntityContainer
    {
        #region Fields

        private string name;
        private string type;

        private bool enabled;

        private RenderMode renderMode;

        private List<ComponentContainer> components;

        private SerializableDictionary<string, object> extras;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
            }
        }

        public RenderMode RenderMode
        {
            get
            {
                return renderMode;
            }
            set
            {
                renderMode = value;
            }
        }

        public SerializableDictionary<string, object> Extras
        {
            get
            {
                return extras;
            }
            set
            {
                extras = value;
            }
        }

        public List<ComponentContainer> Components
        {
            get
            {
                return components;
            }
            set
            {
                components = value;
            }
        }

        #endregion

        #region Constructors

        public EntityContainer()
        {
            this.components = new List<ComponentContainer>();
            this.renderMode = RenderMode.Default;
            this.extras = new SerializableDictionary<string, object>();
        }

        public EntityContainer(string name, string type)
        {
            this.name = name;
            this.type = type;
            this.components = new List<ComponentContainer>();
            this.renderMode = RenderMode.Default;
            this.extras = new SerializableDictionary<string, object>();
        }

        #endregion

        #region Methods

        public Entity CreateEntity(Scene scene)
        {
            Entity entity = Assets.Activator.CreateInstance<Entity>(Type, Name, scene);

            entity.Components.Clear();

            foreach (ComponentContainer container in Components)
            {
                container.CreateComponent(entity);
            }

            entity.Enabled = enabled;
            entity.RenderMode = renderMode;

            ExtrasHelper.SetExtras(entity, extras);

            return entity;
        }

        public static EntityContainer GetContainer(Entity entity)
        {
            EntityContainer container = new EntityContainer();

            container.Name = entity.Name;
            container.Type = entity.GetType().FullName;
            container.Enabled = entity.Enabled;
            container.RenderMode = entity.RenderMode;

            container.Extras = ExtrasHelper.GetExtras(entity);

            foreach (Component component in entity.Components)
            {
                container.Components.Add(ComponentContainer.GetContainer(component));
            }

            return container;
        }

        #endregion
    }
}
