﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DevEngine3.Systems;
using DevEngine3.Interfaces;
using DevEngine3.Helpers;
using System.IO;

namespace DevEngine3.Containers
{
    [Serializable, XmlRoot("Scene")]
    public class SceneContainer : IAsset<SceneContainer>
    {
        #region Fields

        private string name;
        private string type;

        private SceneModifiers modifiers;

        private List<EntityContainer> entities;

        private SerializableDictionary<string, object> extras;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public SceneModifiers Modifiers
        {
            get
            {
                return modifiers;
            }
            set
            {
                modifiers = value;
            }
        }

        public List<EntityContainer> Entities
        {
            get
            {
                return entities;
            }
            set
            {
                entities = value;
            }
        }

        public SerializableDictionary<string, object> Extras
        {
            get
            {
                return extras;
            }
            set
            {
                extras = value;
            }
        }

        #endregion

        #region Constructors

        public SceneContainer()
        {
            this.modifiers = new SceneModifiers();
            this.entities = new List<EntityContainer>();
            this.extras = new SerializableDictionary<string, object>();
        }

        public SceneContainer(string name, string type, SceneModifiers modifiers, List<EntityContainer> entities)
        {
            this.name = name;
            this.type = type;
            this.modifiers = modifiers;
            this.entities = entities;
            this.extras = new SerializableDictionary<string, object>();
        }

        #endregion

        #region Methods

        public Scene CreateScene()
        {
            Scene scene = Assets.Activator.CreateInstance<Scene>(type, name, modifiers);

            foreach (EntityContainer container in entities)
            {
                container.CreateEntity(scene);
            }

            ExtrasHelper.SetExtras(scene, extras);

            return scene;
        }

        public static SceneContainer GetContainer(Scene scene)
        {
            SceneContainer container = new SceneContainer();

            container.Name = scene.Name;
            container.Type = scene.GetType().FullName;

            container.Extras = ExtrasHelper.GetExtras(scene);

            container.Modifiers = scene.Modifiers;

            foreach (Entity entity in scene.Entities)
            {
                container.Entities.Add(EntityContainer.GetContainer(entity));
            }

            return container;
        }

        public SceneContainer FromStream(Stream stream)
        {
            return Assets.Xml.Read<SceneContainer>(stream);
        }

        public Stream ToStream()
        {
            return Assets.Xml.ToStream(this);
        }

        #endregion
    }
}
