﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevEngine3.Systems;

namespace DevEngine3
{
    public partial class ServerForm : Form, IServerInterface
    {
        #region Fields

        private Multiplayer.Server server;

        #endregion

        #region Properties
        #endregion

        #region Constructors

        public ServerForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        public void Initialize(Multiplayer.Server server, object[] arguments)
        {
            this.server = server;

            Application.Run(this);
        }

        public void OnClientConnected(Client client)
        {
        }

        public void OnClientDisconnected(Client client)
        {
        }

        public void OnRunningSlow()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(OnRunningSlow));
                return;
            }

            listBox1.Items.Add("Running slow ):");
        }

        public void Tick(int delta)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action<int>(Tick), delta);
                return;
            }

            listBox1.Items.Add(delta);
        }

        #endregion
    }
}
