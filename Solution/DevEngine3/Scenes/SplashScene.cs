﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevEngine3;
using DevEngine3.Systems;
using DevEngine3.Globals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DevEngine3
{
    public class SplashScene : Scene
    {
        #region Fields

        private Scene scene;
        private double startTime;

        private Entity entity;

        #endregion

        #region Properties
        #endregion

        #region Constructors

        public SplashScene(Scene scene)
        {
            this.scene = scene;
        }

        #endregion

        #region Methods

        public override void Setup()
        {
            Modifiers.ClearColor = Color.WhiteSmoke;

            entity = new Entity("splash");
            entity.AddComponent(new Renderer(Assets.Load<Texture2D>("DevEngine3.Splash.png")));

            entity.Transform.Size = GameInfo.VirtualResolution;

            entity.GetComponent<Renderer>().Color = new Color(Color.White, 0);

            base.Setup();
        }

        public override void Update(GameTime gameTime)
        {
            if (startTime == 0)
            {
                startTime = gameTime.TotalGameTime.TotalSeconds;
            }

            double delta = gameTime.TotalGameTime.TotalSeconds - startTime;

            base.Update(gameTime);

            if (delta > 1f && delta < 1.5f)
            {
                entity.GetComponent<Renderer>().Color = new Color(Color.White, MathHelper.Lerp(0, 1, (float)(delta - 1) / 0.5f));
            }
            else if (delta > 3.0f)
            {
                entity.GetComponent<Renderer>().Color = new Color(Color.White, MathHelper.Lerp(1, 0, (float)(delta - 3) / 0.5f));
            }

            if (delta > 4)
            {
                SceneManager.ChangeScene(scene);
            }
        }

        #endregion
    }
}
