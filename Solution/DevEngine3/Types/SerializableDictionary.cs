﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Xml;

namespace DevEngine3
{
    [XmlRoot("Dictionary")]
    public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
    {
        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Constructors

        public SerializableDictionary()
        {
            return;
        }

        public SerializableDictionary(Dictionary<TKey, TValue> original)
        {
            foreach (KeyValuePair<TKey, TValue> pair in original)
            {
                this.Add(pair.Key, pair.Value);
            }
        }

        #endregion

        #region Methods

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            bool wasEmpty = reader.IsEmptyElement;
            reader.Read();

            if (wasEmpty)
                return;

            while (reader.NodeType != XmlNodeType.EndElement)
            {
                reader.ReadStartElement("Item");

                reader.MoveToContent();

                reader.MoveToAttribute("Type");
                string keyType = reader.GetAttribute("Type");

                XmlSerializer keySerializer = new XmlSerializer(Type.GetType(keyType));

                reader.ReadStartElement("Key");

                TKey key = (TKey)keySerializer.Deserialize(reader);
                reader.ReadEndElement();

                reader.MoveToContent();

                reader.MoveToAttribute("Type");
                string valueType = reader.GetAttribute("Type");

                reader.ReadStartElement("Value");

                if (valueType != null)
                {
                    Type type = Type.GetType(valueType);

                    XmlSerializer valueSerializer = new XmlSerializer(type);

                    TValue value = (TValue)valueSerializer.Deserialize(reader);
                    reader.ReadEndElement();

                    Add(key, value);
                }

                reader.ReadEndElement();
                reader.MoveToContent();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));

            foreach (TKey key in this.Keys)
            {
                TValue value = this[key];

                writer.WriteStartElement("Item");

                writer.WriteStartElement("Key");
                writer.WriteAttributeString("Type", key.GetType().AssemblyQualifiedName);

                keySerializer.Serialize(writer, key);
                writer.WriteEndElement();

                writer.WriteStartElement("Value");

                if (value != null)
                {
                    writer.WriteAttributeString("Type", value.GetType().AssemblyQualifiedName);

                    XmlSerializer valueSerializer = new XmlSerializer(value.GetType());
                    valueSerializer.Serialize(writer, value);
                }

                writer.WriteEndElement();

                writer.WriteEndElement();
            }
        }

        #endregion
    }
}