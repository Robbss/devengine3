﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public class Matrix2
    {
        #region Fields

        private float m00;
        private float m01;
        private float m10;
        private float m11;

        #endregion

        #region Properties

        public static Vector2 operator *(Matrix2 m, Vector2 v)
        {
            return new Vector2(m.m00 * v.X + m.m01 * v.Y, m.m10 * v.X + m.m11 * v.Y);
        }

        #endregion

        #region Constructors

        public Matrix2(float radians)
        {
            float c = (float)Math.Cos(radians);
            float s = (float)Math.Sin(radians);

            m00 = c;
            m01 = -s;
            m10 = s;
            m11 = c;
        }

        public Matrix2(float a, float b, float c, float d)
        {
            m00 = a;
            m01 = b;
            m10 = c;
            m11 = d;
        }

        #endregion

        #region Methods

        public void Set(float radians)
        {
            float c = (float)Math.Cos(radians);
            float s = (float)Math.Sin(radians);

            m00 = c;
            m01 = -s;
            m10 = s;
            m11 = c;
        }

        public Matrix2 Transpose()
        {
            return new Matrix2(m00, m10, m01, m11);
        }

        #endregion
    }
}
