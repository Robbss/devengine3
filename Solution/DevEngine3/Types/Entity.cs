﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevEngine3.Containers;
using DevEngine3.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DevEngine3
{
    [RequireComponent(typeof(Transform))]
    public class Entity
    {
        #region Fields 

        private string name;
        private bool enabled = true;

        private Scene scene;

        private RenderMode renderMode;

        private ComponentCollection components;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
            }
        }

        public Scene Scene
        {
            get
            {
                return scene;
            }
            set
            {
                scene = value;
            }
        }

        public ComponentCollection Components
        {
            get
            {
                return components;
            }
            set
            {
                components = value;
            }
        }

        public RenderMode RenderMode
        {
            get
            {
                return renderMode;
            }
            set
            {
                Scene.Invalidate(this, value);
                renderMode = value;
            }
        }

        public Transform Transform
        {
            get
            {
                return GetComponent<Transform>();
            }
        }

        #endregion

        #region Constructors

        public Entity(string name)
        {
            this.name = name;
            this.scene = SceneManager.CurrentScene;

            this.renderMode = RenderMode.Default;
            this.components = new ComponentCollection(this);

            this.scene.AddEntity(this);
        }

        public Entity(string name, Scene scene)
        {
            this.name = name;
            this.scene = scene;

            this.renderMode = RenderMode.Default;
            this.components = new ComponentCollection(this);

            if (this.scene != null)
            {
                this.scene.AddEntity(this);
            }
        }

        #endregion

        #region Methods

        public virtual void Setup()
        {
            return;
        }

        public virtual void Update(GameTime gameTime)
        {
            foreach (IUpdateable component in components.Updateable)
            {
                if ((component as Component).Enabled)
                {
                    component.Update(gameTime);
                }
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            foreach (IDrawable component in components.Drawable)
            {
                if ((component as Component).Enabled)
                {
                    component.Draw(spriteBatch);
                }
            }
        }

        public virtual T AddComponent<T>() where T : Component, new()
        {
            return components.AddComponent<T>();
        }

        public virtual Component AddComponent(Component component)
        {
            return components.AddComponent(component);
        }

        public virtual void RemoveComponent<T>() where T : Component
        {
            components.RemoveComponent<T>();
        }

        public virtual void RemoveComponent(Component component)
        {
            components.RemoveComponent(component);
        }

        public virtual T GetComponent<T>() where T : Component
        {
            return components.GetComponent<T>();
        }

        public virtual void Destroy()
        {
            Scene.Destroy(this);
        }

        public virtual Coroutine StartCoroutine(IEnumerator enumerator)
        {
            return Scene.StartCoroutine(this, enumerator);
        }

        public virtual Coroutine StartCoroutine(string name, IEnumerator enumerator)
        {
            return Scene.StartCoroutine(this, name, enumerator);
        }

        public virtual bool StopCoroutine(Coroutine coroutine)
        {
            return Scene.StopCoroutine(coroutine);
        }

        public virtual bool StopCoroutine(string name)
        {
            return Scene.StopCoroutine(this, name);
        }

        public static void Destroy(Entity entity)
        {
            Scene.Current.Destroy(entity);
        }

        public static Entity Find(string name)
        {
            return SceneManager.CurrentScene.FindEntity(name);
        }

        #endregion
    }
}
