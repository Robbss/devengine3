﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public class Manifold
    {
        #region Fields

        private RigidBody a;
        private RigidBody b;

        public float penetration;

        public Vector2 normal;

        public Vector2[] contacts;
        public int contact_count;

        public float e;

        public float df;
        public float sf;

        private Action<Manifold, RigidBody, RigidBody>[][] dispatch = new Action<Manifold, RigidBody, RigidBody>[][]
        {
            new Action<Manifold, RigidBody, RigidBody>[]{ CollisionHelper.CircleToCircle, CollisionHelper.CircleToPolygon },
            new Action<Manifold, RigidBody, RigidBody>[]{ CollisionHelper.PolygonToCircle, CollisionHelper.PolygonToPolygon },
        };

        #endregion

        #region Properties
        #endregion

        #region Constructors

        public Manifold(RigidBody a, RigidBody b)
        {
            this.a = a;
            this.b = b;

            contacts = new Vector2[10];
        }

        #endregion

        #region Methods

        public void Solve()
        {
            dispatch[(int)a.shape.Type][(int)b.shape.Type](this, a, b);
        }

        public void Initialize()
        {
            e = MathHelper.Min(a.restitution, b.restitution);

            sf = (float)Math.Sqrt(a.staticFriction * b.staticFriction);
            df = (float)Math.Sqrt(a.dynamicFriction * b.dynamicFriction);

            for (int i = 0; i < contact_count; i++)
            {
                Vector2 ra = contacts[i] - a.position;
                Vector2 rb = contacts[i] - b.position;

                Vector2 rv = b.velocity + rb.Cross(b.angularVelocity) - a.velocity - ra.Cross(a.angularVelocity);

                if (rv.LengthSquared() < (Physics.Dt * Physics.Gravity).LengthSquared() + Physics.Epsilon)
                {
                    e = 0.0f;
                }
            }
        }

        public void ApplyImpulse()
        {
            if (Physics.Equal(a.im + b.im, 0.0f))
            {
                InfiniteMassCorrection();
                return;
            }

            for (int i = 0; i < contact_count; i++)
            {
                Vector2 ra = contacts[i] - a.position;
                Vector2 rb = contacts[i] - b.position;

                Vector2 rv = b.velocity + rb.Cross(b.angularVelocity) - a.velocity - ra.Cross(a.angularVelocity);

                float contactVel = Vector2.Dot(rv, normal);

                if (contactVel > 0)
                {
                    return;
                }

                float raCrossN = ra.Cross(normal);
                float rbCrossN = rb.Cross(normal);

                float invMassSum = a.im + b.im + (raCrossN * raCrossN) * a.iI + (rbCrossN * rbCrossN) * b.iI;

                float j = -(1.0f + e) * contactVel;

                j /= invMassSum;
                j /= (float)contact_count;

                Vector2 impulse = normal * j;

                a.ApplyImpulse(-impulse, ra);
                b.ApplyImpulse(impulse, rb);

                rv = b.velocity + rb.Cross(b.angularVelocity) - a.velocity - ra.Cross(a.angularVelocity);

                Vector2 t = rv - (normal * Vector2.Dot(rv, normal));
                t.Normalize();

                float jt = -Vector2.Dot(rv, normal);

                jt /= invMassSum;
                jt /= (float)contact_count;

                if (Physics.Equal(jt, 0.0f))
                {
                    return;
                }

                Vector2 tangentImpulse;

                if (Math.Abs(jt) < j * sf)
                {
                    tangentImpulse = t * jt;
                }
                else
                {
                    tangentImpulse = t * -j * df;
                }

                a.ApplyImpulse(-tangentImpulse, ra);
                b.ApplyImpulse(tangentImpulse, rb);
            }
        }

        public void PositionalCorrection()
        {
            float k_slop = 0.05f;
            float percent = 0.4f;

            Vector2 correction = (MathHelper.Max(penetration - k_slop, 0.0f) / (a.im + b.im)) * normal * percent;

            a.position -= correction * a.im;
            b.position += correction * b.im;

            if (correction != Vector2.Zero)
            {

            }
        }

        public void InfiniteMassCorrection()
        {
            a.velocity = Vector2.Zero;
            b.velocity = Vector2.Zero;
        }



        #endregion
    }
}
