﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevEngine3.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DevEngine3.Systems;
using DevEngine3.Globals;
using DevEngine3.Containers;

namespace DevEngine3
{
    public class Scene : IAsset<Scene>
    {
        #region Fields

        #region State Modifiers

        private bool hasSetup = false;

        #endregion

        private string name;
        private SceneModifiers modifiers;

        private List<Entity> entities;
        private Dictionary<RenderMode, List<Entity>> sortedEntities;

        private Stack<Entity> toAdd;
        private Stack<Entity> toDestroy;

        private List<Coroutine> coroutines;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public SceneModifiers Modifiers
        {
            get
            {
                return modifiers;
            }
            set
            {
                modifiers = value;
            }
        }

        public List<Entity> Entities
        {
            get
            {
                return entities;
            }
            set
            {
                entities = value;
            }
        }

        public Camera Camera
        {
            get
            {
                foreach (Entity entity in entities)
                {
                    if (entity is Camera)
                    {
                        return entity as Camera;
                    }
                }

                return null;
            }
        }

        public GraphicsDevice GraphicsDevice
        {
            get
            {
                return SceneManager.GraphicsDevice;
            }
        }

        public static Scene Current
        {
            get
            {
                return SceneManager.CurrentScene;
            }
        }

        #endregion

        #region Constructors

        public Scene()
        {
            this.modifiers = new SceneModifiers();

            this.entities = new List<Entity>();
            this.sortedEntities = new Dictionary<RenderMode, List<Entity>>();

            this.toAdd = new Stack<Entity>();
            this.toDestroy = new Stack<Entity>();

            this.coroutines = new List<Coroutine>();
        }

        public Scene(string name)
        {
            this.name = name;
            this.modifiers = new SceneModifiers();

            this.entities = new List<Entity>();
            this.sortedEntities = new Dictionary<RenderMode, List<Entity>>();

            this.toAdd = new Stack<Entity>();
            this.toDestroy = new Stack<Entity>();

            this.coroutines = new List<Coroutine>();
        }

        public Scene(string name, SceneModifiers modifiers)
        {
            this.name = name;
            this.modifiers = modifiers;

            this.entities = new List<Entity>();
            this.sortedEntities = new Dictionary<RenderMode, List<Entity>>();

            this.toAdd = new Stack<Entity>();
            this.toDestroy = new Stack<Entity>();

            this.coroutines = new List<Coroutine>();
        }

        public Scene(string name, SceneModifiers modifiers, List<Entity> entities)
        {
            this.name = name;
            this.modifiers = modifiers;

            this.entities = entities;
            this.sortedEntities = new Dictionary<RenderMode, List<Entity>>();

            this.toAdd = new Stack<Entity>();
            this.toDestroy = new Stack<Entity>();

            this.coroutines = new List<Coroutine>();
        }

        #endregion

        #region Methods

        public virtual void Setup()
        {
            if (!hasSetup)
            {
                foreach (Entity entity in entities)
                {
                    entity.Setup();
                }
            }

            if (Camera == null)
            {
                Camera camera = new Camera("main-camera");
                camera.Setup();
            }

            hasSetup = true;
        }

        public virtual void Update(GameTime gameTime)
        {
            bool dirty = toDestroy.Count > 0;

            while (toDestroy.Count > 0)
            {
                entities.Remove(toDestroy.Pop());
            }

            while (toAdd.Count > 0)
            {
                Entity entity = toAdd.Pop();

                entities.Add(entity);

                if (!dirty)
                {
                    Invalidate(entity, entity.RenderMode);
                }
            }

            if (dirty)
            {
                Invalidate();
            }

            foreach (Entity entity in entities)
            {
                if (entity.Enabled)
                {
                    entity.Update(gameTime);
                }
            }

            foreach (Coroutine coroutine in coroutines)
            {
                coroutine.MoveNext();
            }

            coroutines.RemoveAll(o => !o.Active);
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (modifiers.Clear)
            {
                GraphicsDevice.Clear(modifiers.ClearColor);
            }

            Camera camera = Camera;

            if (sortedEntities.ContainsKey(RenderMode.Manual))
            {
                foreach (Entity entity in sortedEntities[RenderMode.Manual])
                {
                    if (entity.Enabled && camera.IsVisible(entity))
                    {
                        entity.Draw(spriteBatch);
                    }
                }
            }

            if (sortedEntities.ContainsKey(RenderMode.Default))
            {
                spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, transformMatrix: Camera.Matrix);

                foreach (Entity entity in sortedEntities[RenderMode.Default])
                {
                    if (entity.Enabled && camera.IsVisible(entity))
                    {
                        entity.Draw(spriteBatch);
                    }
                }

                spriteBatch.End();
            }

            if (sortedEntities.ContainsKey(RenderMode.Static))
            {
                spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);

                foreach (Entity entity in sortedEntities[RenderMode.Static])
                {
                    if (entity.Enabled && camera.IsVisible(entity))
                    {
                        entity.Draw(spriteBatch);
                    }
                }

                spriteBatch.End();
            }

            if (sortedEntities.ContainsKey(RenderMode.Shader))
            {
                foreach (Entity entity in sortedEntities[RenderMode.Shader])
                {
                    if (entity.Enabled && camera.IsVisible(entity))
                    {
                        Shader shader = entity.GetComponent<Shader>();

                        spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, effect: shader.Effect, transformMatrix: Camera.Matrix);

                        foreach (string technique in shader.Techniques)
                        {
                            shader.Effect.CurrentTechnique = shader.Effect.Techniques[technique];

                            foreach (EffectPass pass in shader.Effect.CurrentTechnique.Passes)
                            {
                                pass.Apply();
                                entity.Draw(spriteBatch);
                            }
                        }

                        spriteBatch.End();
                    }
                }
            }
        }

        public virtual void AddEntity(Entity entity)
        {
            if (hasSetup)
            {
                entity.Setup();
                toAdd.Push(entity);
            }
            else
            {
                entities.Add(entity);
                Invalidate(entity, entity.RenderMode);
            }
        }

        public virtual void Destroy(Entity entity)
        {
            toDestroy.Push(entity);
        }

        public virtual void Invalidate()
        {
            sortedEntities.Clear();

            foreach (Entity entity in entities)
            {
                if (!sortedEntities.ContainsKey(entity.RenderMode))
                {
                    sortedEntities.Add(entity.RenderMode, new List<Entity>());
                }

                sortedEntities[entity.RenderMode].Add(entity);
            }
        }

        public virtual void Invalidate(Entity entity, RenderMode renderMode)
        {
            if (sortedEntities.ContainsKey(entity.RenderMode))
            {
                sortedEntities[entity.RenderMode].Remove(entity);
            }

            if (!sortedEntities.ContainsKey(renderMode))
            {
                sortedEntities.Add(renderMode, new List<Entity>());
            }

            sortedEntities[renderMode].Add(entity);
        }

        public virtual Entity FindEntity(string name)
        {
            foreach (Entity entity in entities)
            {
                if (entity.Name == name)
                {
                    return entity;
                }
            }

            return null;
        }

        public virtual Coroutine StartCoroutine(Entity entity, IEnumerator enumerator)
        {
            Coroutine coroutine = new Coroutine(entity, enumerator);

            coroutines.Add(coroutine);

            return coroutine;
        }

        public virtual Coroutine StartCoroutine(Entity entity, string name, IEnumerator enumerator)
        {
            Coroutine coroutine = new Coroutine(entity, name, enumerator);

            coroutines.Add(coroutine);

            return coroutine;
        }

        public virtual bool StopCoroutine(Coroutine coroutine)
        {
            return coroutines.Remove(coroutine);
        }

        public virtual bool StopCoroutine(Entity entity, string name)
        {
            return StopCoroutine(coroutines.FirstOrDefault(o => o.Entity == entity && o.Name == name));
        }

        #region IAsset<Scene>

        public Scene FromStream(Stream stream)
        {
            return Assets.Xml.Read<SceneContainer>(stream).CreateScene();
        }

        public Stream ToStream()
        {
            return Assets.Xml.ToStream(SceneContainer.GetContainer(this));
        }

        #endregion

        #endregion
    }
}
