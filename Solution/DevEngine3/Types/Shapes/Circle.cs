﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevEngine3
{
    public class Circle : Shape
    {
        #region Fields
        #endregion

        #region Properties

        public override ShapeType Type
        {
            get
            {
                return ShapeType.Circle;
            }
        }

        #endregion

        #region Constructors

        public Circle(float radius) : base()
        {
            Radius = radius;
        }

        #endregion

        #region Methods

        public override Shape Clone()
        {
            return new Circle(Radius);
        }

        public override void Initialize()
        {
            ComputeMass(1.0f);
        }

        public override void ComputeMass(float density)
        {
            Body.m = (float)Math.PI * Radius * Radius * density;
            Body.im = (Body.m > 0.0f) ? 1.0f / Body.m : 0.0f;
            Body.I = Body.m * Radius * Radius;
            Body.iI = (Body.I > 0.0f) ? 1.0f / Body.I : 0.0f;
        }

        #endregion
    }
}
