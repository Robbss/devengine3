﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public class Shape
    {
        #region Enums

        public enum ShapeType : int
        {
            Circle,
            Poly
        }

        #endregion

        #region Fields

        private RigidBody body;
        private float radius;
        private Matrix2 u;

        #endregion

        #region Properties

        public RigidBody Body
        {
            get
            {
                return body;
            }
            set
            {
                body = value;
            }
        }

        public float Radius
        {
            get
            {
                return radius;
            }
            set
            {
                radius = value;
            }
        }

        public Matrix2 U
        {
            get
            {
                return u;
            }
            set
            {
                u = value;
            }
        }

        public virtual ShapeType Type
        {
            get;
        }

        #endregion

        #region Constructors

        public Shape()
        {
            u = new Matrix2(0);
        }

        #endregion

        #region Methods

        public virtual Shape Clone()
        {
            return null;
        }

        public virtual void Initialize()
        {
            return;
        }

        public virtual void ComputeMass(float density)
        {
            return;
        }

        public virtual void SetOrient(float radians)
        {
            return;
        }

        #endregion
    }
}
