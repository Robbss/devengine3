﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public class Polygon : Shape
    {
        #region Fields

        private const int MaxVertexCount = 64;

        public int m_vertexCount;
        public Vector2[] m_vertices;
        public Vector2[] m_normals;

        #endregion

        #region Properties

        public override ShapeType Type
        {
            get
            {
                return ShapeType.Poly;
            }
        }

        #endregion

        #region Constructors

        public Polygon() : base()
        {
            m_vertices = new Vector2[MaxVertexCount];
            m_normals = new Vector2[MaxVertexCount];
        }

        #endregion

        #region Methods

        public override void Initialize()
        {
            ComputeMass(0.001f);
        }

        public override Shape Clone()
        {
            Polygon polygon = new Polygon();
            polygon.U = U;

            for (int i = 0; i < m_vertexCount; i++)
            {
                polygon.m_vertices[i] = m_vertices[i];
                polygon.m_normals[i] = m_normals[i];
            }

            polygon.m_vertexCount = m_vertexCount;

            return polygon;
        }

        public override void ComputeMass(float density)
        {
            Vector2 c = Vector2.Zero;

            float area = 0.0f;
            float I = 0.0f;

            float k_inv3 = 1.0f / 3.0f;

            for (int i = 0; i < m_vertexCount; i++)
            {
                Vector2 p1 = m_vertices[i];
                int i2 = i + 1 < m_vertexCount ? i + 1 : 0;
                Vector2 p2 = m_vertices[i2];

                float D = p1.Cross(p2);
                float triangleArea = 0.5f * D;

                area += triangleArea;

                c += triangleArea * k_inv3 * (p1 + p2);

                float intx2 = p1.X * p1.X + p2.X * p1.X + p2.X * p2.X;
                float inty2 = p1.Y * p1.Y + p2.Y * p1.Y + p2.Y * p2.Y;

                I += (0.25f * k_inv3 * D) * (intx2 + inty2);
            }

            c *= 1.0f / area;

            for (int i = 0; i < m_vertexCount; i++)
            {
                m_vertices[i] -= c;
            }

            Body.m = density * area;
            Body.im = (Body.m > 0.0f) ? 1.0f / Body.m : 0.0f;

            Body.I = I * density;
            Body.iI = (Body.I > 0.0f) ? 1.0f / Body.I : 0.0f;
        }

        public override void SetOrient(float radians)
        {
            U.Set(radians);
        }

        public void SetBox(float hw, float hh)
        {
            m_vertexCount = 4;

            m_vertices[0] = new Vector2(-hw, -hh);
            m_vertices[1] = new Vector2(hw, -hh);
            m_vertices[2] = new Vector2(hw, hh);
            m_vertices[3] = new Vector2(-hw, hh);

            m_normals[0] = new Vector2(0.0f, -1.0f);
            m_normals[1] = new Vector2(1.0f, 0.0f);
            m_normals[2] = new Vector2(0.0f, 1.0f);
            m_normals[3] = new Vector2(-1.0f, 0.0f);            
        }

        public void Set(Vector2[] vertices)
        {
            if (vertices.Length < 2 || vertices.Length > MaxVertexCount)
            {
                throw new GenericException("Polygon", "Incorrect vertices count");
            }

            int rightMost = 0;
            float highestXCoord = vertices[0].X;

            for (int i = 1; i < vertices.Length; i++)
            {
                float x = vertices[i].X;

                if (x > highestXCoord)
                {
                    highestXCoord = x;
                    rightMost = i;
                }
                else if (x == highestXCoord)
                {
                    if (vertices[i].Y < vertices[rightMost].Y)
                    {
                        rightMost = i;
                    }
                }
            }

            int[] hull = new int[MaxVertexCount];
            int outCount = 0;
            int indexHull = rightMost;

            for (;;)
            {
                hull[outCount] = indexHull;

                int nextHullIndex = 0;

                for (int i = 1; i < vertices.Length; i++)
                {
                    if (nextHullIndex == indexHull)
                    {
                        nextHullIndex = i;
                        continue;
                    }

                    Vector2 e1 = vertices[nextHullIndex] - vertices[hull[outCount]];
                    Vector2 e2 = vertices[i] - vertices[hull[outCount]];

                    float c = e1.Cross(e2);

                    if (c < 0.0f)
                    {
                        nextHullIndex = i;
                    }

                    if (c == 0.0f && e2.LengthSquared() > e1.LengthSquared())
                    {
                        nextHullIndex = i;
                    }
                }

                ++outCount;
                indexHull = nextHullIndex;

                if (nextHullIndex == rightMost)
                {
                    m_vertexCount = outCount;
                    break;
                }
            }

            for (int i = 0; i < m_vertexCount; i++)
            {
                m_vertices[i] = vertices[hull[i]];
            }

            for (int i = 0; i < m_vertexCount; i++)
            {
                int i2 = i + 1 < m_vertexCount ? i + 1 : 0;
                Vector2 face = m_vertices[i2] - m_vertices[i];

                if (face.LengthSquared() > Physics.Epsilon * Physics.Epsilon)
                {
                    throw new GenericException("Polygon", "wtf");
                }

                m_normals[i] = new Vector2(face.Y, -face.X);
                m_normals[i].Normalize();
            }
        }

        public Vector2 GetSupport(Vector2 direction)
        {
            float bestProjection = -float.MaxValue;
            Vector2 bestVertex = Vector2.Zero;

            for (int i = 0; i < m_vertexCount; i++)
            {
                Vector2 v = m_vertices[i];
                float projection = Vector2.Dot(v, direction);

                if (projection > bestProjection)
                {
                    bestVertex = v;
                    bestProjection = projection;
                }
            }

            return bestVertex;
        }

        #endregion
    }
}
