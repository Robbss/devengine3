﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevEngine3
{
    public class EditorViewItem
    {
        #region Fields       

        private string valuePath;
        private Type valueType;

        private EditorDisplayMode displayMode;
        private object[] parameters;

        #endregion

        #region Properties        

        public string ValuePath
        {
            get
            {
                return valuePath;
            }
        }

        public Type ValueType
        {
            get
            {
                return valueType;
            }
        }

        public EditorDisplayMode DisplayMode
        {
            get
            {
                return displayMode;
            }
        }

        public object[] Parameters
        {
            get
            {
                return parameters;
            }
        }

        #endregion

        #region Constructors

        public EditorViewItem(string valuePath, Type valueType, EditorDisplayMode displayMode, params object[] parameters)
        {
            this.valuePath = valuePath;
            this.valueType = valueType;

            this.displayMode = displayMode;
            this.parameters = parameters;
        }

        #endregion

        #region Methods
        #endregion
    }
}
