﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevEngine3.Systems;

namespace DevEngine3
{
    public class ComponentCollection : IEnumerable<Component>
    {
        #region Fields

        private Entity entity;

        private Dictionary<Type, Component> components;

        private List<IDrawable> drawable;
        private List<IUpdateable> updateable;

        #endregion

        #region Properties

        public List<IDrawable> Drawable
        {
            get
            {
                return drawable;
            }
            set
            {
                drawable = value;
            }
        }

        public List<IUpdateable> Updateable
        {
            get
            {
                return updateable;
            }
            set
            {
                updateable = value;
            }
        }

        #endregion

        #region Constructors

        public ComponentCollection(Entity entity)
        {
            this.components = new Dictionary<Type, Component>();

            this.drawable = new List<IDrawable>();
            this.updateable = new List<IUpdateable>();

            this.entity = entity;

            List<Type> required = new List<Type>();

            foreach (Attribute attribute in entity.GetType().GetCustomAttributes(true))
            {
                RequireComponentAttribute attr = attribute as RequireComponentAttribute;

                if (attr != null)
                {
                    required.AddRange(attr.RequiredTypes);
                }
            }

            foreach (Type type in required)
            {
                AddComponent(Assets.Activator.CreateInstance<Component>(type));
            }
        }

        #endregion

        #region Methods

        public T AddComponent<T>() where T : Component, new()
        {
            return AddComponent(new T()) as T;
        }

        public Component AddComponent(Component component)
        {
            Type type = component.GetType();

            if (components.ContainsKey(type))
            {
                throw new GenericException("ComponentCollection", "Component of same type already added");
            }

            components.Add(type, component);

            if (component is IDrawable)
            {
                drawable.Add(component as IDrawable);
            }

            if (component is IUpdateable)
            {
                updateable.Add(component as IUpdateable);
            }

            component.Entity = entity;
            component.Setup();

            return component;
        }

        public void RemoveComponent<T>() where T : Component
        {
            if (components.ContainsKey(typeof(T)))
            {
                components.Remove(typeof(T));

                Invalidate();
            }
        }

        public void RemoveComponent(Component component)
        {
            if (components.ContainsKey(component.GetType()))
            {
                components.Remove(component.GetType());

                Invalidate();
            }
        }

        public T GetComponent<T>() where T : Component
        {
            Component result;

            if (components.TryGetValue(typeof(T), out result))
            {
                return result as T;
            }
            else
            {
                foreach (KeyValuePair<Type, Component> pairs in components)
                {
                    if (typeof(T).IsAssignableFrom(pairs.Key))
                    {
                        return pairs.Value as T;
                    }
                }
            }

            return null;
        }

        public void Invalidate()
        {
            drawable.Clear();
            updateable.Clear();

            foreach (Component component in this)
            {
                if (component is IDrawable)
                {
                    drawable.Add(component as IDrawable);
                }

                if (component is IUpdateable)
                {
                    updateable.Add(component as IUpdateable);
                }
            }
        }

        public void Clear()
        {
            components.Clear();
            Invalidate();
        }

        public IEnumerator<Component> GetEnumerator()
        {
            return components.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
