﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevEngine3
{
    public class Component
    {
        #region Fields

        private Entity entity;
        private bool enabled;

        #endregion

        #region Properties

        public Entity Entity
        {
            get
            {
                return entity;
            }
            set
            {
                entity = value;
            }
        }

        public Scene Scene
        {
            get
            {
                return entity.Scene;
            }
        }

        public Transform Transform
        {
            get
            {
                return entity.Transform;
            }
        }

        public bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
            }
        }

        #endregion

        #region Constructors

        public Component()
        {
            enabled = true;
        }

        #endregion

        #region Methods

        public virtual void Setup()
        {
            return;
        }

        public virtual T AddComponent<T>() where T : Component, new()
        {
            return entity.Components.AddComponent<T>();
        }

        public virtual Component AddComponent(Component component)
        {
            return entity.Components.AddComponent(component);
        }

        public virtual void RemoveComponent<T>() where T : Component
        {
            entity.Components.RemoveComponent<T>();
        }

        public virtual void RemoveComponent(Component component)
        {
            entity.Components.RemoveComponent(component);
        }

        public virtual T GetComponent<T>() where T : Component
        {
            return entity.Components.GetComponent<T>();
        }

        public virtual void Destroy()
        {
            entity.RemoveComponent(this);
        }

        protected virtual Coroutine StartCoroutine(IEnumerator enumerator)
        {
            return Entity.StartCoroutine(enumerator);
        }

        protected virtual Coroutine StartCoroutine(string name, IEnumerator enumerator)
        {
            return Entity.StartCoroutine(name, enumerator);
        }

        protected virtual bool StopCoroutine(Coroutine coroutine)
        {
            return Entity.StopCoroutine(coroutine);
        }

        protected virtual bool StopCoroutine(string name)
        {
            return Entity.StopCoroutine(name);
        }

        #endregion
    }
}
