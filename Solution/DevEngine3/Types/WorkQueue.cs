﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace DevEngine3
{
    public class WorkQueue
    {
        #region Fields

        private Queue<Action> work;
        private object workLock = new object();

        private Queue<Action> pending;
        private object pendingLock = new object();

        private ManualResetEvent waitHandle;
        private ManualResetEvent workHandle;

        private Thread[] workThreads;
        private bool threaded;

        #endregion

        #region Properties
        #endregion

        #region Constructors

        public WorkQueue()
        {
            work = new Queue<Action>();
            pending = new Queue<Action>();
        }

        public WorkQueue(int threads) : this()
        {
            workThreads = new Thread[threads];

            for (int i = 0; i < threads; i++)
            {
                workThreads[i] = new Thread(PerformWorkThread);
            }

            waitHandle = new ManualResetEvent(false);
            workHandle = new ManualResetEvent(false);

            threaded = true;
        }

        ~WorkQueue()
        {
            if (threaded)
            {
                threaded = false;

                work.Clear();
                pending.Clear();

                waitHandle.Set();

                for (int i = 0; i < workThreads.Length; i++)
                {
                    workThreads[i].Join();
                }
            }
        }

        #endregion

        #region Methods

        public void Enqueue(Action item)
        {
            lock (pendingLock)
            {
                pending.Enqueue(item);
            }
        }

        public void PerformWork()
        {
            PerformWork(int.MaxValue);
        }

        public void PerformWork(int duration)
        {
            Stopwatch sw = Stopwatch.StartNew();

            lock (workLock)
            {
                lock (pendingLock)
                {
                    while (pending.Count > 0)
                    {
                        work.Enqueue(pending.Dequeue());
                    }
                }

                if (!threaded)
                {
                    while (sw.ElapsedMilliseconds < duration)
                    {
                        work.Dequeue().Invoke();
                    }

                    sw.Stop();
                }
            }

            if (threaded)
            {
                workHandle.Reset();
                waitHandle.Set();

                workHandle.WaitOne(duration - (int)sw.ElapsedMilliseconds);
                waitHandle.Reset();

                sw.Stop();
            }
        }

        private void PerformWorkThread()
        {
            Action action;

            while (threaded)
            {
                waitHandle.WaitOne();

                lock (workLock)
                {
                    if (work.Count > 0)
                    {
                        action = work.Dequeue();
                    }
                    else
                    {
                        action = null;
                    }
                }

                if (action == null)
                {
                    while (!waitHandle.Reset())
                    {
                    }

                    while (!workHandle.Set())
                    {
                    }

                    continue;
                }
                else
                {
                    action.Invoke();
                }
            }
        }

        #endregion
    }
}
