﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public class Sizef
    {
        #region Fields

        private float width;
        private float height;

        #endregion

        #region Properties

        public float Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }

        public float Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
            }
        }

        public static Sizef Empty
        {
            get
            {
                return new Sizef();
            }
        }

        public static Sizef One
        {
            get
            {
                return new Sizef(1, 1);
            }
        }

        public static implicit operator Sizef(Vector2 vector)
        {
            return new Sizef(vector.X, vector.Y);
        }

        public static implicit operator Vector2(Sizef sizef)
        {
            return new Vector2(sizef.Width, sizef.Height);
        }

        public static implicit operator Sizef(Size size)
        {
            return new Sizef(size.Width, size.Height);
        }

        public static Sizef operator *(Sizef sizef, float value)
        {
            return new Sizef(sizef.Width * value, sizef.Height * value);
        }

        public static Sizef operator /(Sizef sizef, float value)
        {
            return new Sizef(sizef.Width / value, sizef.Height / value);
        }

        #endregion

        #region Constructors

        public Sizef()
        {
            this.width = 0;
            this.height = 0;
        }

        public Sizef(float width, float height)
        {
            this.width = width;
            this.height = height;
        }

        #endregion

        #region Methods

        public Point ToPoint()
        {
            return new Point((int)width, (int)height);
        }

        #endregion
    }
}
