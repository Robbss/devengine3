﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public class World
    {
        #region Fields

        public List<RigidBody> bodies;
        public List<Manifold> contacts;

        #endregion

        #region Properties
        #endregion

        #region Constructors

        public World()
        {
            bodies = new List<RigidBody>();
            contacts = new List<Manifold>();
        }

        #endregion

        #region Methods

        public void IntegrateForces(RigidBody b, float dt)
        {
            if (b.im == 0.0f)
            {
                return;
            }

            b.velocity += (b.force * b.im + Physics.Gravity) * (dt / 2.0f);
            b.angularVelocity += b.torque * b.iI * (dt / 2.0f);
        }

        public void IntegrateVelocity(RigidBody b, float dt)
        {
            if (b.im == 0.0f)
            {
                return;
            }

            b.position += b.velocity * dt;
            b.orientation += b.angularVelocity * dt;

            IntegrateForces(b, dt);
        }

        public void Step()
        {
            contacts.Clear();

            for (int i = 0; i < bodies.Count; i++)
            {
                RigidBody A = bodies[i];

                for (int j = i + 1; j < bodies.Count; j++)
                {
                    RigidBody B = bodies[j];

                    if (A.im == 0.0f && B.im == 0.0f)
                    {
                        continue;
                    }

                    Manifold m = new Manifold(A, B);
                    m.Solve();

                    if (m.contact_count > 0)
                    {
                        contacts.Add(m);
                    }
                }
            }

            for (int i = 0; i < bodies.Count; i++)
            {
                IntegrateForces(bodies[i], Physics.Dt);
            }

            for (int i = 0; i < contacts.Count; i++)
            {
                contacts[i].Initialize();
            }

            for (int i = 0; i < Physics.Iterations; i++)
            {
                for (int j = 0; j < contacts.Count; j++)
                {
                    contacts[j].ApplyImpulse();
                }
            }

            for (int i = 0; i < bodies.Count; i++)
            {
                IntegrateVelocity(bodies[i], Physics.Dt);
            }

            for (int i = 0; i < contacts.Count; i++)
            {
                contacts[i].PositionalCorrection();
            }

            for (int i = 0; i < bodies.Count; i++)
            {
                RigidBody b = bodies[i];

                b.force = Vector2.Zero;
                b.torque = 0;
            }
        }

        #endregion
    }
}
