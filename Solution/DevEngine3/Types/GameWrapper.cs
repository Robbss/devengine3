﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevEngine3.Globals;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public class GameWrapper : Game
    {
        #region Fields

        private bool hide = false;
        private Form form;

        #endregion

        #region Properties
        #endregion

        #region Events

        public event Action OnLoadContent;

        public event Action<GameTime> OnUpdate;
        public event Action<GameTime> OnDraw;

        #endregion

        #region Constructors

        public GameWrapper()
        {
            IsMouseVisible = GameInfo.MouseVisible;
        }

        internal GameWrapper(Control control)
        {
        }

        #endregion

        #region Methods

        public void Run(bool hide)
        {
            this.hide = hide;
            this.form = (Form)Form.FromHandle(Window.Handle);

            this.form.Top = (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height - GameInfo.Resolution.Height) / 2;
            this.form.Left = (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width - GameInfo.Resolution.Width) / 2;

            Run();
        }

        #region Overrides

        protected override void LoadContent()
        {
            OnLoadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            if (hide && form.Visible)
            {
                form.Hide();
            }

            OnUpdate(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            OnDraw(gameTime);
        }

        #endregion

        #endregion
    }
}
