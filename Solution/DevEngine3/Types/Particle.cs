﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public class Particle : Entity
    {
        #region Fields

        private Vector2 velocity;
        private Vector2 acceleration;

        private float lifetime;
        private float lifetimeElapsed;

        private float rotationSpeed;

        #endregion

        #region Properties

        public Vector2 Velocity
        {
            get
            {
                return velocity;
            }
            set
            {
                velocity = value;
            }
        }

        public Vector2 Acceleration
        {
            get
            {
                return acceleration;
            }
            set
            {
                acceleration = value;
            }
        }

        public float Lifetime
        {
            get
            {
                return lifetime;
            }
            set
            {
                lifetime = value;
            }
        }

        public float LifetimeElapsed
        {
            get
            {
                return lifetimeElapsed;
            }
            set
            {
                lifetimeElapsed = value;
            }
        }

        public float RotationSpeed
        {
            get
            {
                return rotationSpeed;
            }
            set
            {
                rotationSpeed = value;
            }
        }

        #endregion

        #region Constructors

        public Particle(string name, Scene scene) : base(name, scene)
        {
            Enabled = false;
            Transform.Size = new Sizef(5, 5);
        }

        #endregion

        #region Methods

        public void Initialize(Vector2 position, Vector2 velocity, Vector2 acceleration, float lifetime, float scale, float rotationSpeed)
        {
            Transform.Position = position;
            Transform.Scale = new Vector2(scale);
            Transform.Rotation = RandomHelper.Between(0, MathHelper.TwoPi);

            this.velocity = velocity;
            this.acceleration = acceleration;

            this.lifetime = lifetime;
            this.lifetimeElapsed = 0;

            this.rotationSpeed = rotationSpeed;

            Enabled = true;
        }

        public override void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            velocity += acceleration * dt;

            Transform.Position += velocity * dt;
            Transform.Rotation += rotationSpeed * dt;

            lifetimeElapsed += dt;

            if (lifetimeElapsed > lifetime)
            {
                Enabled = false;
            }

            base.Update(gameTime);
        }

        #endregion
    }
}
