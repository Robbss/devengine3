﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace DevEngine3
{
    public class Listener
    {
        #region Fields

        private List<Client> clients;
        private TcpListener listener;

        private bool active = false;
        private object sync = new object();

        #endregion

        #region Properties

        public bool Active
        {
            get
            {
                return active;
            }
        }

        #endregion

        #region Events

        public event Action<Client> OnClientConnected;
        public event Action<Client> OnClientDisconnected;

        #endregion

        #region Constructors

        public Listener(int port)
        {
            this.clients = new List<Client>();
            this.listener = new TcpListener(IPAddress.Any, port);
        }

        #endregion

        #region Methods

        public void BeginListen()
        {
            Thread thread = new Thread(Listen);
            thread.Start();
        }

        public void EndListen()
        {
            active = false;
        }

        public void Listen()
        {
            List<Task> tasks = new List<Task>();
            active = true;

            listener.Start();

            while (active)
            {
                if (listener.Pending())
                {
                    tasks.Add(AcceptClient());
                }
                else
                {
                    Thread.Sleep(200);
                }
            }
        }

        private async Task AcceptClient()
        {
            TcpClient tcpClient = await listener.AcceptTcpClientAsync();
            int count;

            lock (sync)
            {
                count = clients.Count;
            }

            RecieveClient(new Client(tcpClient));
        }

        private void RecieveClient(Client client)
        {
            lock (sync)
            {
                clients.Add(client);
            }

            OnClientConnected?.Invoke(client);

            client.OnDisconnect += Client_OnDisconnect;
        }

        private void Client_OnDisconnect(Client client)
        {
            lock (sync)
            {
                clients.Remove(client);
            }

            OnClientDisconnected?.Invoke(client);
        }

        #endregion
    }
}
