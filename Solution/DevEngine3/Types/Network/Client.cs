﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;

namespace DevEngine3
{
    public class Client
    {
        #region Fields

        private TcpClient client;
        private NetworkStream stream;

        private Thread listenThread;
        private bool active = false;

        private TimeSpan timeout = new TimeSpan(0, 0, 10);

        #endregion

        #region Events

        public event Action<Client, Packet> OnPacketRecieved;
        public event Action<Client> OnDisconnect;

        #endregion

        #region Properties

        public bool Active
        {
            get
            {
                return active;
            }
        }

        public bool IsConnected
        {
            get
            {
                try
                {
                    if (client != null && client.Client != null && client.Client.Connected)
                    {
                        if (client.Client.Poll(0, SelectMode.SelectRead))
                        {
                            byte[] buff = new byte[1];
                            if (client.Client.Receive(buff, SocketFlags.Peek) == 0)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
        }

        #endregion

        #region Constructors

        public Client(TcpClient client)
        {
            this.client = client;
            this.client.NoDelay = true;

            this.stream = client.GetStream();
        }

        #endregion

        #region Methods

        public void BeginListen()
        {
            listenThread = new Thread(Listen);
            listenThread.Start();
        }

        public void EndListen()
        {
            active = false;
            listenThread.Join();
        }

        private void Listen()
        {
            active = true;

            while (active)
            {
                try
                {
                    OnPacketRecieved?.Invoke(this, ReadPacket());
                }
                catch (Exception ex)
                {
                    if (ex is IOException || ex is ObjectDisposedException)
                    {
                        active = false;
                        OnDisconnect?.Invoke(this);
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        public Packet ReadPacket()
        {
            DateTime start = DateTime.Now;

            byte[] buffer = new byte[4];
            int recv = 0;
            int tries = 0;

            while (recv < buffer.Length)
            {
                int delta = stream.Read(buffer, recv, buffer.Length - recv);
                recv += delta;

                if (DateTime.Now - start > timeout || tries > 10)
                {
                    if (IsConnected)
                    {
                        tries = 0;
                        start = DateTime.Now;
                    }
                    else
                    {
                        throw new IOException();
                    }
                }

                if (delta == 0)
                {
                    tries++;
                }
            }

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(buffer);
            }

            int length = BitConverter.ToInt32(buffer, 0);

            if (length < 0 || length > int.MaxValue)
            {
                throw new ArgumentOutOfRangeException();
            }

            buffer = new byte[length];
            recv = 0;
            tries = 0;

            while (recv < buffer.Length)
            {
                int delta = stream.Read(buffer, recv, buffer.Length - recv);
                recv += delta;

                if (DateTime.Now - start > timeout || tries > 10)
                {
                    if (IsConnected)
                    {
                        tries = 0;
                        start = DateTime.Now;
                    }
                    else
                    {
                        throw new IOException();
                    }
                }

                if (delta == 0)
                {
                    tries++;
                }
            }

            byte[] output;

            using (MemoryStream outms = new MemoryStream())
            {
                using (MemoryStream inms = new MemoryStream(buffer))
                {
                    using (GZipStream gzip = new GZipStream(inms, CompressionMode.Decompress))
                    {
                        byte[] bytes = new byte[4096];

                        int count;

                        while ((count = gzip.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            outms.Write(bytes, 0, count);
                        }
                    }
                }

                output = outms.ToArray();
            }

            return Packet.FromBytes(output);
        }

        public Task<bool> SendAsync(Packet packet)
        {
            return Task.Run(() => { return Send(packet); });
        }

        public Task<bool> SendAsync(string header, params object[] payload)
        {
            return SendAsync(new Packet(header, payload));
        }

        public bool Send(string header, params object[] payload)
        {
            return Send(new Packet(header, payload));
        }

        public bool Send(Packet packet)
        {
            byte[] input = packet.ToBytes();
            byte[] output;

            using (MemoryStream ms = new MemoryStream())
            {
                using (GZipStream compressStream = new GZipStream(ms, CompressionMode.Compress))
                {
                    compressStream.Write(input, 0, input.Length);
                }

                output = ms.ToArray();
            }

            return Send(output);
        }

        public bool Send(byte[] bytes)
        {
            byte[] length = BitConverter.GetBytes(bytes.Length);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(length);
            }

            byte[] message = length.Concat(bytes).ToArray();

            try
            {
                stream.Write(message, 0, message.Length);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public void Close()
        {
            if (active)
            {
                EndListen();
            }

            client.Close();
            stream.Dispose();
        }

        public static Client Connect(string address, int port)
        {
            TcpClient tcpClient = new TcpClient();
            tcpClient.Connect(address, port);

            if (tcpClient.Connected)
            {
                return new Client(tcpClient);
            }

            return null;
        }

        #endregion
    }
}
