﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DevEngine3
{
    [Serializable]
    public class Packet
    {
        #region Fields

        private string header;
        private object[] payload;

        private static BinaryFormatter binaryFormatter;

        #endregion

        #region Properties

        public string Header
        {
            get
            {
                return header;
            }
        }

        public object[] Payload
        {
            get
            {
                return payload;
            }
        }

        private static BinaryFormatter BinaryFormatter
        {
            get
            {
                if (binaryFormatter == null)
                {
                    binaryFormatter = new BinaryFormatter();
                }

                return binaryFormatter;
            }
        }

        #endregion

        #region Constructors

        public Packet(string header)
        {
            this.header = header;
            this.payload = null;
        }

        public Packet(string header, List<object> payload)
        {
            this.header = header;
            this.payload = payload.ToArray();
        }

        public Packet(string header, params object[] payload)
        {
            this.header = header;
            this.payload = payload;
        }

        #endregion

        #region Methods

        public T FromIndex<T>(int index)
        {
            if (index < 0 || index >= payload.Length)
            {
                throw new ArgumentOutOfRangeException();
            }

            object value = payload[index];

            if (value is T)
            {
                return (T)value;
            }

            throw new InvalidCastException();
        }

        public byte[] ToBytes()
        {
            byte[] result;

            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter.Serialize(ms, this);
                result = ms.ToArray();
            }

            return result;
        }

        public static Packet FromBytes(byte[] bytes)
        {
            Packet result = null;

            using (MemoryStream ms = new MemoryStream(bytes))
            {
                result = (Packet)BinaryFormatter.Deserialize(ms);
            }

            return result;
        }

        #endregion
    }
}
