﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public class Camera : Entity
    {
        #region Fields

        private Matrix matrix;

        #endregion

        #region Properties

        public Matrix Matrix
        {
            get
            {
                return matrix;
            }
        }

        public static Camera Current
        {
            get
            {
                return Scene.Current.Camera;
            }
        }

        #endregion

        #region Constructors

        public Camera(string name) : base(name)
        {
        }

        public Camera(string name, Scene scene) : base(name, scene)
        {
        }

        #endregion

        #region Methods

        public override void Setup()
        {
            Invalidate();
            base.Setup();
        }

        public void Invalidate()
        {
            Matrix translationMatrix = Matrix.CreateTranslation(-Transform.Position.X, -Transform.Position.Y, 0);
            Matrix rotationMatrix = Matrix.CreateRotationZ(MathHelper.ToRadians(Transform.Rotation));
            Matrix scaleMatrix = Matrix.CreateScale(Transform.Scale.Width, Transform.Scale.Height, 1);

            matrix = translationMatrix * rotationMatrix * (Screen.ScaleMatrix * scaleMatrix);

            Transform.Size = new Sizef(Screen.VirtualWidth, Screen.VirtualHeight);
        }

        public void CenterAt(Transform transform)
        {
            Transform.Center = transform.Center;
            Invalidate();
        }

        public void Translate(float x, float y)
        {
            Transform.Translate(x, y);
            Invalidate();
        }

        public void Translate(Vector2 vector)
        {
            Translate(vector.X, vector.Y);
        }

        public Vector2 ScreenPointToWorld(Vector2 point)
        {
            return Vector2.Transform(point, Matrix.Invert(matrix));
        }

        public bool IsVisible(Entity entity)
        {
            return entity.Transform.Bounds.Intersects(Transform.Bounds);
        }

        #endregion
    }
}
