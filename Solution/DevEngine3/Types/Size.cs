﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public class Size
    {
        #region Fields

        private int width;
        private int height;

        #endregion

        #region Properties

        public int Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }

        public int Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
            }
        }

        public static Size Empty
        {
            get
            {
                return new Size();
            }
        }

        public static implicit operator Size(Point point)
        {
            return new Size(point.X, point.Y);
        }

        public static implicit operator Point(Size size)
        {
            return new Point(size.Width, size.Height);
        }

        public static Size operator *(Size size, int value)
        {
            return new Size(size.Width * value, size.Height * value);
        }

        public static Size operator /(Size size, int value)
        {
            return new Size(size.Width / value, size.Height / value);
        }

        #endregion

        #region Constructors

        public Size()
        {
            this.width = 0;
            this.height = 0;
        }

        public Size(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        #endregion

        #region Methods
        #endregion
    }
}
