﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevEngine3
{
    public class Coroutine
    {
        #region Fields

        private string name;
        private Entity entity;

        private IEnumerator enumerator;

        private bool active = true;

        #endregion

        #region Properties

        public string Name
        {
            get
            {
                return name;
            }
        }

        public Entity Entity
        {
            get
            {
                return entity;
            }
        }

        public IEnumerator Enumerator
        {
            get
            {
                return enumerator;
            }
        }

        public bool Active
        {
            get
            {
                return active;
            }
        }

        #endregion

        #region Constructors

        public Coroutine(Entity entity, IEnumerator enumerator)
        {
            this.name = Guid.NewGuid().ToString();
            this.entity = entity;

            this.enumerator = enumerator;
        }

        public Coroutine(Entity entity, string name, IEnumerator enumerator)
        {
            this.name = name;
            this.entity = entity;

            this.enumerator = enumerator;
        }

        #endregion

        #region Methods

        public void MoveNext()
        {
            if (!enumerator.MoveNext())
            {
                active = false;
            }
        }

        #endregion
    }
}
