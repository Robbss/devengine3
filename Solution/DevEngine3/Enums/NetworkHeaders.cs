﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevEngine3
{
    public enum NetworkHeaders : byte
    {
        BEGIN_HEADER,
        END_HEADER,

        BEGIN_PAYLOAD,
        END_PAYLOAD
    }
}
