﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace DevEngine3
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
    public class SerializeExtraFromAttribute : Attribute
    {
        #region Fields

        private string[] set;
        private string[] get;

        #endregion

        #region Properties
        #endregion

        #region Constructors

        public SerializeExtraFromAttribute(string set, string get)
        {
            this.set = set.Split('.');
            this.get = get.Split('.');
        }

        #endregion

        #region Methods

        public object GetValue<T>(T obj)
        {
            PropertyInfo property = obj.GetType().GetProperty(get[0], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            object instance = obj;

            for (int i = 1; i < get.Length; i++)
            {
                if (property == null || instance == null)
                {
                    return null;
                }

                instance = property.GetValue(instance);

                if (instance == null)
                {
                    return null;
                }

                property = instance.GetType().GetProperty(get[i], BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            }

            return property.GetValue(instance);
        }

        public void SetValue<T>(T obj, object value)
        {
            PropertyInfo property = null;
            object instance = obj;

            for (int i = 1; i < set.Length - 1; i++)
            {
                instance = property.GetValue(instance);
                property = instance.GetType().GetProperty(set[i]);

                if (property == null || instance == null)
                {
                    return;
                }
            }

            instance.GetType().GetMethod(set[set.Length - 1]).Invoke(instance, new object[] { value });
        }

        #endregion
    }
}
