﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevEngine3
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
    public sealed class EditorDisplayAttribute : Attribute
    {
        #region Fields

        private string property;
        private EditorDisplayMode displayMode;

        #endregion

        #region Properties

        public string Property
        {
            get
            {
                return property;
            }
        }

        public EditorDisplayMode DisplayMode
        {
            get
            {
                return displayMode;
            }
        }

        #endregion

        #region Constructors

        public EditorDisplayAttribute(EditorDisplayMode displayMode)
        {
            this.property = string.Empty;
            this.displayMode = displayMode;
        }

        public EditorDisplayAttribute(string property, EditorDisplayMode displayMode)
        {
            this.property = property;
            this.displayMode = displayMode;
        }

        #endregion

        #region Methods
        #endregion
    }
}
