﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevEngine3
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public sealed class RequireComponentAttribute : Attribute
    {
        #region Fields

        private Type[] requiredTypes;

        #endregion

        #region Properties

        public Type[] RequiredTypes
        {
            get
            {
                return requiredTypes;
            }
        }

        #endregion

        #region Constructors

        public RequireComponentAttribute(params Type[] requiredTypes)
        {
            this.requiredTypes = requiredTypes;
        }

        #endregion

        #region Methods
        #endregion
    }
}
