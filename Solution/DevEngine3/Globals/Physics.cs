﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public static class Physics
    {
        #region Fields

        private static float epsilon = 0.0001f;

        private static int steps = 0;
        private static int iterations = 100;

        private static float dt = 1.0f / 60.0f;

        private static Vector2 gravity = new Vector2(0, 200);

        #endregion

        #region Properties

        public static float Epsilon
        {
            get
            {
                return epsilon;
            }
        }

        public static int Iterations
        {
            get
            {
                return iterations;
            }
        }

        public static Vector2 Gravity
        {
            get
            {
                return gravity;
            }
        }

        public static float Dt
        {
            get
            {
                return dt;
            }
        }

        public static int Steps
        {
            get
            {
                return steps;
            }
        }

        #endregion

        #region Constructors
        #endregion

        #region Methods

        public static bool Equal(float a, float b)
        {
            return Math.Abs(a - b) <= Epsilon;
        }

        #endregion
    }
}
