﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevEngine3.Globals
{
    public static class EngineInfo
    {
        #region Fields

        private const string engineName = "DevEngine3";

        #endregion

        #region Properties

        public static string EngineName
        {
            get
            {
                return engineName;
            }
        }

        #endregion

        #region Constructors
        #endregion

        #region Methods
        #endregion
    }
}
