﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DevEngine3
{
    public static class Input
    {
        #region Fields

        private static KeyboardState previousKeyboard;
        private static KeyboardState currentKeyboard;

        private static MouseState previousMouse;
        private static MouseState currentMouse;

        #endregion

        #region Properties

        public static Vector2 MousePosition
        {
            get
            {
                return Vector2.Clamp(currentMouse.Position.ToVector2(), Vector2.Zero, new Vector2(Screen.Width, Screen.Height));
            }
        }

        public static Vector2 WorldMousePosition
        {
            get
            {
                return ScreenPointToWorld(MousePosition);
            }
        }

        #endregion

        #region Constructors

        static Input()
        {
            currentKeyboard = Keyboard.GetState();
            currentMouse = Mouse.GetState();
        }

        #endregion

        #region Methods

        public static void Update(GameTime gameTime)
        {
            previousKeyboard = currentKeyboard;
            currentKeyboard = Keyboard.GetState();

            previousMouse = currentMouse;
            currentMouse = Mouse.GetState();
        }

        public static bool GetKey(Keys key)
        {
            return currentKeyboard.IsKeyDown(key);
        }

        public static bool GetKeyDown(Keys key)
        {
            return currentKeyboard.IsKeyDown(key) && previousKeyboard.IsKeyUp(key);
        }

        public static bool GetKeyUp(Keys key)
        {
            return currentKeyboard.IsKeyUp(key) && previousKeyboard.IsKeyDown(key);
        }

        public static bool GetMouseButton(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.Left:
                    return currentMouse.LeftButton == ButtonState.Pressed;

                case MouseButtons.Right:
                    return currentMouse.RightButton == ButtonState.Pressed;

                case MouseButtons.Middle:
                    return currentMouse.MiddleButton == ButtonState.Pressed;
            }

            throw new GenericException("Input", "Unsupported MouseButton");
        }

        public static bool GetMouseButtonDown(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.Left:
                    return currentMouse.LeftButton == ButtonState.Pressed && previousMouse.LeftButton == ButtonState.Released;

                case MouseButtons.Right:
                    return currentMouse.RightButton == ButtonState.Pressed && previousMouse.RightButton == ButtonState.Released;

                case MouseButtons.Middle:
                    return currentMouse.MiddleButton == ButtonState.Pressed && previousMouse.MiddleButton == ButtonState.Released;
            }

            throw new GenericException("Input", "Unsupported MouseButton");
        }

        public static bool GetMouseButtonUp(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.Left:
                    return currentMouse.LeftButton == ButtonState.Released && previousMouse.LeftButton == ButtonState.Pressed;

                case MouseButtons.Right:
                    return currentMouse.RightButton == ButtonState.Released && previousMouse.RightButton == ButtonState.Pressed;

                case MouseButtons.Middle:
                    return currentMouse.MiddleButton == ButtonState.Released && previousMouse.MiddleButton == ButtonState.Pressed;
            }

            throw new GenericException("Input", "Unsupported MouseButton");
        }

        public static Vector2 ScreenPointToWorld(Vector2 point)
        {
            return Camera.Current.ScreenPointToWorld(point);
        }

        #endregion
    }
}
