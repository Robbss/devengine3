﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevEngine3.Containers;
using DevEngine3.Systems;

namespace DevEngine3.Globals
{
    public static class GameInfo
    {
        #region Fields

        private static string name = $"{EngineInfo.EngineName} Game";
        private static string contentPath = "Content";

        private static bool mouseVisible = true;
        private static bool fullScreen = false;

        private static Size virtualResolution = new Size(1280, 720);
        private static Size resolution = new Size(1280, 720);

        private static string sceneManager = "DevEngine3.SceneManagers.DefaultSceneManager";
        private static string scene = "DevEngine3.Scene";

        #endregion

        #region Properties

        public static string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public static string ContentPath
        {
            get
            {
                return contentPath;
            }
            set
            {
                contentPath = value;
            }
        }

        public static bool MouseVisible
        {
            get
            {
                return mouseVisible;
            }
            set
            {
                mouseVisible = value;
            }
        }

        public static bool FullScreen
        {
            get
            {
                return fullScreen;
            }
            set
            {
                fullScreen = value;
            }
        }

        public static Size VirtualResolution
        {
            get
            {
                return virtualResolution;
            }
            set
            {
                virtualResolution = value;
            }
        }

        public static Size Resolution
        {
            get
            {
                return resolution;
            }
            set
            {
                resolution = value;
            }
        }

        public static string SceneManager
        {
            get
            {
                return sceneManager;
            }
            set
            {
                sceneManager = value;
            }
        }

        public static string Scene
        {
            get
            {
                return scene;
            }
            set
            {
                scene = value;
            }
        }

        #endregion

        #region Constructors
        #endregion

        #region Methods

        public static void Load(GameInfoContainer gameInfoContainer)
        {
            name = gameInfoContainer.Name;
            contentPath = gameInfoContainer.ContentPath;

            mouseVisible = gameInfoContainer.MouseVisible;
            fullScreen = gameInfoContainer.FullScreen;

            virtualResolution = gameInfoContainer.VirtualResolution;
            resolution = gameInfoContainer.Resolution;

            sceneManager = gameInfoContainer.SceneManager;
            scene = gameInfoContainer.Scene;
        }

        public static void Save(string path)
        {
            Assets.Xml.Write(new GameInfoContainer(name, contentPath, mouseVisible, fullScreen, virtualResolution, resolution, sceneManager, scene), path);
        }

        #endregion
    }
}
