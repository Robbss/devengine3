﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevEngine3.Systems;
using DevEngine3.Globals;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public static class Screen
    {
        #region Properties

        public static int Width
        {
            get
            {
                return SceneManager.GraphicsDevice.PresentationParameters.BackBufferWidth;
            }
        }

        public static int Height
        {
            get
            {
                return SceneManager.GraphicsDevice.PresentationParameters.BackBufferHeight;
            }
        }

        public static int VirtualWidth
        {
            get
            {
                return GameInfo.VirtualResolution.Width;
            }
        }

        public static int VirtualHeight
        {
            get
            {
                return GameInfo.VirtualResolution.Height;
            }
        }

        public static Matrix ScaleMatrix
        {
            get
            {
                return Matrix.CreateScale(new Vector3(ScaleRatioWidth, ScaleRatioHeight, 1));
            }
        }

        public static float ScaleRatioWidth
        {
            get
            {
                return (float)Width / (float)VirtualWidth;
            }
        }

        public static float ScaleRatioHeight
        {
            get
            {
                return (float)Height / (float)VirtualHeight;
            }
        }

        #endregion

        #region Methods
        #endregion
    }
}
