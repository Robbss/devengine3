﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevEngine3.Interfaces;
using DevEngine3.Globals;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DevEngine3.SceneManagers
{
    public class DefaultSceneManager : ISceneManager
    {
        #region Fields

        private Scene scene;
        private Scene next;

        #endregion

        #region Properties

        public Scene Current
        {
            get
            {
                return scene;
            }
            set
            {
                scene = value;
            }
        }

        #endregion

        #region Constructors
        #endregion

        #region Methods

        public void ChangeScene(Scene scene)
        {
            if (this.scene == null)
            {
                this.scene = scene;
            }
            else
            {
                next = scene;
            }
        }

        public bool Start(ref GameWrapper gameWrapper, ref GraphicsDeviceManager graphicsDeviceManager)
        {
            return false;
        }

        public void Setup()
        {
            scene.Setup();
        }

        public void Update(GameTime gameTime)
        {
            scene.Update(gameTime);

            if (next != null)
            {
                scene = next;
                scene.Setup();

                next = null;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            scene.Draw(spriteBatch);
        }

        #endregion
    }
}
