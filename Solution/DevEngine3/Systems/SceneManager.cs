﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DevEngine3.Interfaces;
using DevEngine3.Globals;
using DevEngine3.SceneManagers;
using System.Reflection;
using DevEngine3.Containers;
using Microsoft.Xna.Framework.Content;

namespace DevEngine3.Systems
{
    public static class SceneManager
    {
        #region Fields

        private static ISceneManager sceneManager;
        private static GameWrapper gameWrapper;

        private static GraphicsDeviceManager graphicsDeviceManager;
        private static SpriteBatch spriteBatch;

        #endregion

        #region Properties

        public static ISceneManager Current
        {
            get
            {
                return sceneManager;
            }
            private set
            {
                sceneManager = value;
            }
        }

        public static Scene CurrentScene
        {
            get
            {
                return sceneManager.Current;
            }
        }

        public static GraphicsDeviceManager GraphicsDeviceManager
        {
            get
            {
                return graphicsDeviceManager;
            }
        }

        public static GraphicsDevice GraphicsDevice
        {
            get
            {
                return graphicsDeviceManager.GraphicsDevice;
            }
        }

        public static ContentManager ContentManager
        {
            get
            {
                return gameWrapper.Content;
            }
        }

        #endregion

        #region Methods        

        public static void Play()
        {
            SceneContainer scene = Assets.Load<SceneContainer>(GameInfo.Scene);

            if (scene == null)
            {
                throw new GenericException("SceneManager", "Starting scene could not be found");
            }

            Play(scene);
        }

        public static void Play(SceneContainer scene)
        {
            Type type = Type.GetType(GameInfo.SceneManager, false, true);

            if (type == null)
            {
                throw new GenericException("SceneManager", "The chosen SceneManager could not be found");
            }

            if (!typeof(ISceneManager).IsAssignableFrom(type))
            {
                throw new GenericException("SceneManager", "The chosen SceneManager does not implement ISceneManager");
            }

            Play((ISceneManager)Activator.CreateInstance(type), scene);
        }

        public static void Play(ISceneManager sceneManager)
        {
            SceneContainer scene = Assets.Load<SceneContainer>(GameInfo.Scene);

            if (scene == null)
            {
                throw new GenericException("SceneManager", "Starting scene could not be found");
            }

            Play(sceneManager, scene);
        }

        public static void Play(ISceneManager sceneManager, SceneContainer scene)
        {
            gameWrapper = new GameWrapper();
            gameWrapper.Window.Title = GameInfo.Name;

            graphicsDeviceManager = new GraphicsDeviceManager(gameWrapper);

            gameWrapper.OnLoadContent += Setup;
            gameWrapper.OnUpdate += Update;
            gameWrapper.OnDraw += Draw;

            graphicsDeviceManager.PreferredBackBufferWidth = GameInfo.Resolution.Width;
            graphicsDeviceManager.PreferredBackBufferHeight = GameInfo.Resolution.Height;

            graphicsDeviceManager.IsFullScreen = GameInfo.FullScreen;

            Current = sceneManager;

            bool hide = Current.Start(ref gameWrapper, ref graphicsDeviceManager);

            graphicsDeviceManager.ApplyChanges();

#if DEBUG
            Current.ChangeScene(scene.CreateScene());
#else
            Current.ChangeScene(new SplashScene(scene.CreateScene()));
#endif

            gameWrapper.Run(hide);
        }

        public static void ChangeSceneManager(ISceneManager sceneManager, bool moveScene = true)
        {
            Scene currentScene = CurrentScene;

            Current = sceneManager;
            Current.ChangeScene(currentScene);
        }

        public static T GetSceneManagerAs<T>()
        {
            return (T)Current;
        }

        public static void ChangeScene(Scene scene)
        {
            Current.ChangeScene(scene);
        }

        public static void ChangeScene(string scene)
        {
            ChangeScene(Assets.Load<Scene>(scene));
        }

        #region Privates

        private static void Setup()
        {
            if (spriteBatch == null)
            {
                spriteBatch = new SpriteBatch(GraphicsDevice);
            }

            sceneManager.Setup();
        }

        private static void Update(GameTime gameTime)
        {
            Input.Update(gameTime);

            sceneManager.Update(gameTime);
        }

        private static void Draw(GameTime gameTime)
        {
            sceneManager.Draw(spriteBatch);
        }

        #endregion

        #endregion
    }
}
