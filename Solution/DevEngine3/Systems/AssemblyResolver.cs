﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using DevEngine3.Globals;

namespace DevEngine3.Systems
{
    public static class AssemblyResolver
    {
        #region Fields

        private static List<string> searchPaths;

        #endregion

        #region Properties

        public static List<string> SearchPaths
        {
            get
            {
                return searchPaths;
            }
        }

        #endregion

        #region Constructors

        static AssemblyResolver()
        {
            searchPaths = new List<string>();
            searchPaths.Add("Assemblies");
        }

        #endregion

        #region Methods

        public static void PrepareAssemblyResolver()
        {
            AppDomain.CurrentDomain.AssemblyResolve += Resolve;
        }

        private static Assembly Resolve(object sender, ResolveEventArgs e)
        {
            Assembly assembly;

            if (FindAssemblyFromFile(e.Name, out assembly) || FindAssemblyFromResource(e.Name, out assembly))
            {
                return assembly;
            }
            else
            {
                if (!e.Name.Contains(".XmlSerializers"))
                {
                    throw new GenericException("AssemblyResolver", $"Required Assembly '{e.Name}' couldn't be found");
                }
                else
                {
                    return null;
                }
            }
        }

        private static bool FindAssemblyFromResource(string rawName, out Assembly foundAssembly)
        {
            string assemblyFileName = $"{new AssemblyName(rawName).Name}.dll";

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (!assembly.IsDynamic)
                {
                    string[] manifests = assembly.GetManifestResourceNames();
                    string match = manifests.FirstOrDefault(manifest => manifest.EndsWith(assemblyFileName));

                    if (match != null)
                    {
                        foundAssembly = CreateTempAssemblyFromResource(assemblyFileName, assembly.GetManifestResourceStream(match));
                        return true;
                    }
                }
            }

            foundAssembly = null;
            return false;
        }

        private static Assembly CreateTempAssemblyFromResource(string assemblyName, Stream resource)
        {
            string tempPath = Path.Combine(Path.GetTempPath(), GameInfo.Name);

            if (!Directory.Exists(tempPath))
            {
                Directory.CreateDirectory(tempPath);
            }

            tempPath = Path.Combine(tempPath, assemblyName);

            using (FileStream fs = new FileStream(tempPath, FileMode.OpenOrCreate, FileAccess.Write))
            {
                byte[] buffer = new byte[resource.Length];

                using (resource)
                {
                    resource.Read(buffer, 0, buffer.Length);
                }

                fs.Write(buffer, 0, buffer.Length);
            }

            return Assembly.LoadFrom(tempPath);
        }

        private static bool FindAssemblyFromFile(string rawName, out Assembly foundAssembly)
        {
            string assemblyFileName = $"{new AssemblyName(rawName).Name}.dll";

            foreach (string searchPath in searchPaths)
            {
                string path = Path.Combine(Environment.CurrentDirectory, searchPath);

                if (FindAssemblyInFolder(path, assemblyFileName, out foundAssembly))
                {
                    return true;
                }
            }

            foundAssembly = null;
            return false;
        }

        private static bool FindAssemblyInFolder(string path, string assemblyName, out Assembly assembly)
        {
            if (Directory.Exists(path))
            {
                foreach (string filePath in Directory.GetFiles(path))
                {
                    string fileName = Path.GetFileName(filePath);

                    if (fileName == assemblyName)
                    {
                        assembly = Assembly.LoadFrom(filePath);
                        return true;
                    }
                }
            }

            assembly = null;
            return false;
        }

        #endregion
    }
}
