﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.IO.Compression;
using System.Windows.Forms;
using System.Diagnostics;

namespace DevEngine3.Systems
{
    public static class Multiplayer
    {
        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Methods

        public static void StartServer(IServerInterface serverInterface, int port, int tickRate, params object[] arguments)
        {
            Server server = new Server(port, tickRate);

            server.OnClientConnected += serverInterface.OnClientConnected;
            server.OnClientDisconnected += serverInterface.OnClientDisconnected;

            server.Tick += serverInterface.Tick;
            server.OnRunningSlow += serverInterface.OnRunningSlow;

            server.Start();

            serverInterface.Initialize(server, arguments);
        }

        public static void StartServer<T>(int port, int tickRate, params object[] arguments) where T : IServerInterface, new()
        {
            StartServer(new T(), port, tickRate, arguments);
        }

        #endregion

        public class Server
        {
            #region Fields

            private int port;
            private int tickRate;

            private Listener listener;

            private Thread tickThread;
            private bool tickRunning;

            #endregion

            #region Properties

            public int Port
            {
                get
                {
                    return port;
                }
            }

            public int TickRate
            {
                get
                {
                    return tickRate;
                }
            }

            #endregion

            #region Events

            public event Action<Client> OnClientConnected;
            public event Action<Client> OnClientDisconnected;

            public event Action<int> Tick;
            public event Action OnRunningSlow;

            #endregion

            #region Constructors

            public Server(int port, int tickRate)
            {
                if (tickRate <= 0)
                {
                    throw new ArgumentException("TickRate cannot be less than 1");
                }

                this.port = port;
                this.tickRate = tickRate;
            }

            #endregion

            #region Methods

            public void Start()
            {
                listener = new Listener(port);

                if (OnClientConnected != null)
                {
                    listener.OnClientConnected += OnClientConnected;
                }

                if (OnClientDisconnected != null)
                {
                    listener.OnClientDisconnected += OnClientDisconnected;
                }

                listener.BeginListen();

                tickRunning = true;

                tickThread = new Thread(InternalTick);
                tickThread.Start();
            }

            public void Stop()
            {
                listener.EndListen();

                tickRunning = false;
                tickThread.Join();
            }

            internal void InternalTick()
            {
                Stopwatch stopWatch = Stopwatch.StartNew();
                ManualResetEvent eventHandle = new ManualResetEvent(false);

                int msPerTick = 1000 / tickRate;

                while (tickRunning)
                {
                    int delta = (int)stopWatch.ElapsedMilliseconds;

                    stopWatch.Restart();

                    Tick?.Invoke(delta);

                    if (delta > msPerTick + msPerTick * 2)
                    {
                        OnRunningSlow?.Invoke();
                    }

                    if (stopWatch.ElapsedMilliseconds < msPerTick)
                    {
                        int wait = (int)(msPerTick - stopWatch.ElapsedMilliseconds);

                        if (wait > 0)
                        {
                            eventHandle.WaitOne(wait);
                        }
                    }
                }
            }

            #endregion
        }
    }
}