﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevEngine3.Systems;
using DevEngine3.Containers;
using DevEngine3.Globals;
using DevEngine3.Interfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DevEngine3
{
    public class DGame : IDisposable
    {
        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Constructors

        static DGame()
        {
            AssemblyResolver.PrepareAssemblyResolver();
        }

        public DGame(GameInfoContainer gameInfoContainer)
        {
            GameInfo.Load(gameInfoContainer);
        }

        public DGame(string gameName, string contentPath)
        {
            GameInfo.Name = gameName;
            GameInfo.ContentPath = contentPath;
        }

        #endregion

        #region Methods

        public void Start()
        {
            SceneManager.Play();
        }

        public void Start(SceneContainer scene)
        {
            SceneManager.Play(scene);
        }

        public void Start(ISceneManager sceneManager)
        {
            SceneManager.Play(sceneManager);
        }

        public void Start(SceneContainer scene, ISceneManager sceneManager)
        {
            SceneManager.Play(sceneManager, scene);
        }

        public static void Play(string assetName)
        {
            using (DGame game = new DGame(Assets.Load<GameInfoContainer>(assetName)))
            {
                game.Start();
            }
        }

        public void Dispose()
        {
            return;
        }

        #endregion
    }
}
