﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.IO.Compression;
using DevEngine3.Interfaces.Internal;

namespace DevEngine3.Systems.Internal
{
    public class Xml : IHideObject
    {
        #region Methods

        public T Read<T>(string path)
        {
            T result;

            using (FileStream fs = File.OpenRead(path))
            {
                result = Read<T>(fs);
            }

            return result;
        }

        public T Read<T>(string path, CompressionLevel compressionLevel)
        {
            T result;

            using (FileStream fs = File.OpenRead(path))
            {
                result = Read<T>(fs, compressionLevel);
            }

            return result;
        }

        public T Read<T>(Stream stream)
        {
            T result;

            using (StreamReader reader = new StreamReader(stream))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                result = (T)serializer.Deserialize(reader);
            }

            return result;
        }

        public T Read<T>(Stream stream, CompressionLevel compressionLevel)
        {
            T result;

            using (GZipStream zipStream = new GZipStream(stream, compressionLevel, false))
            {
                result = Read<T>(zipStream);
            }

            return result;
        }

        public void Write<T>(T obj, string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                Write<T>(obj, fs);
            }
        }

        public void Write<T>(T obj, string path, CompressionLevel compressionLevel)
        {
            using (FileStream fs = File.Create(path))
            {
                using (GZipStream gzipStream = new GZipStream(fs, compressionLevel))
                {
                    Write<T>(obj, gzipStream);
                }
            }
        }

        public void Write<T>(T obj, Stream stream)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            serializer.Serialize(stream, obj);
        }

        public void Write<T>(T obj, Stream stream, CompressionLevel compressionLevel)
        {
            using (GZipStream gzipStream = new GZipStream(stream, compressionLevel, true))
            {
                Write<T>(obj, gzipStream);
            }
        }

        public Stream ToStream<T>(T obj)
        {
            MemoryStream ms = new MemoryStream();

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            serializer.Serialize(ms, obj);

            return ms;
        }

        #endregion
    }
}
