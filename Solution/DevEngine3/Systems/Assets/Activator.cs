﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using activator = System.Activator;

namespace DevEngine3.Systems.Internal
{
    public class Activator
    {
        #region Methods

        public T CreateInstance<T>(Type type)
        {
            return (T)activator.CreateInstance(type);
        }

        public T CreateInstance<T>(string type, params object[] args)
        {
            return (T)activator.CreateInstance(FindType(type), args);
        }

        public T CreateInstance<T>()
        {
            return (T)activator.CreateInstance<T>();
        }

        public T CreateInstance<T>(params object[] args)
        {
            return (T)activator.CreateInstance(typeof(T), args);
        }

        public Type FindType(string name)
        {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                Type type = assembly.GetType(name, false);

                if (type != null)
                {
                    return type;
                }
            }

            return null;
        }

        public PropertyInfo[] GetPropertiesWithAttribute<T>(Type type) where T : Attribute
        {
            List<PropertyInfo> properties = new List<PropertyInfo>();

            foreach (PropertyInfo propertyInfo in type.GetProperties())
            {
                T attribute = propertyInfo.GetCustomAttribute<T>(true);

                if (attribute != null)
                {
                    properties.Add(propertyInfo);
                }
            }

            return properties.ToArray();
        }

        /*
        public MemberInfo GetMemberInfoFromPath(Type type, string path)
        {
            if (path.Contains("."))
            {
                string[] names = path.Split('.');
                MemberInfo memberInfo = type.GetProperty(names[0]) as MemberInfo ?? type.GetField(names[0]) as MemberInfo;

                if (memberInfo == null)
                {
                    return null;
                }

                for (int i = 1; i < names.Length; i++)
                {
                    Type value = null;

                    if (memberInfo is PropertyInfo)
                    {
                        value = (memberInfo as PropertyInfo).PropertyType;
                    }
                    else if (memberInfo is FieldInfo)
                    {
                        value = (memberInfo as FieldInfo).FieldType;
                    }

                    memberInfo = value.GetProperty(names[i]) as MemberInfo ?? value.GetField(names[i]) as MemberInfo;
                }

                return memberInfo;
            }

            return null;
        }
        */



        #endregion
    }
}
