﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using DevEngine3.Systems.Internal;
using DevEngine3.Interfaces;
using DevEngine3.Globals;
using Microsoft.Xna.Framework.Graphics;

namespace DevEngine3.Systems
{
    public static class Assets
    {
        #region Fields

        private static Xml xml;
        private static Internal.Activator activator;

        private static Dictionary<string, object> cache = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

        #endregion

        #region Properties

        public static Xml Xml
        {
            get
            {
                return xml;
            }
        }

        public static Internal.Activator Activator
        {
            get
            {
                return activator;
            }
        }

        #endregion

        #region Constructors

        static Assets()
        {
            xml = new Xml();
            activator = new Internal.Activator();
        }

        #endregion

        #region Methods

        public static T Load<T>(string assetName) where T : class
        {
            T result = default(T);

            if (typeof(IAsset<T>).IsAssignableFrom(typeof(T)))
            {
                if (LoadFromCache(assetName, out result) || LoadFromResource(assetName, out result) || LoadFromFile(assetName, out result))
                {
                    cache.Add(assetName, result);
                }
            }
            else
            {
                #region Native Assets

                if (typeof(T) == typeof(Texture2D))
                {
                    Stream stream = null;

                    if (LoadFromCache(assetName, out result))
                    {
                        return result;
                    }
                    else if (LoadFromResource(assetName, out stream) || LoadFromFile(assetName, out stream))
                    {
                        result = Texture2D.FromStream(SceneManager.GraphicsDevice, stream) as T;

                        cache.Add(assetName, result);
                    }

                    return result;
                }

                if (typeof(T) == typeof(Effect))
                {
                    Stream stream = null;

                    if (LoadFromCache(assetName, out result))
                    {
                        return result;
                    }
                    else if (LoadFromResource(assetName, out stream) || LoadFromFile(assetName, out stream))
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            stream.CopyTo(ms);
                            result = new Effect(SceneManager.GraphicsDevice, ms.ToArray()) as T;
                        }

                        cache.Add(assetName, result);
                    }

                    return result;
                }

                if (typeof(T) == typeof(Stream))
                {
                    Stream stream = null;

                    if (LoadFromCache(assetName, out result))
                    {
                        return result;
                    }
                    else if (LoadFromResource(assetName, out stream) || LoadFromFile(assetName, out stream))
                    {
                        cache.Add(assetName, stream);
                    }

                    return stream as T;
                }

                if (typeof(T) == typeof(SpriteFont))
                {
                    if (LoadFromCache(assetName, out result))
                    {
                        return result;
                    }
                    else
                    {
                        Stream stream = null;

                        if (File.Exists(Path.Combine(GameInfo.ContentPath, assetName)))
                        {
                            SceneManager.ContentManager.RootDirectory = GameInfo.ContentPath;
                            result = SceneManager.ContentManager.Load<T>(Path.ChangeExtension(assetName, null));

                            cache.Add(assetName, result);
                        }
                        else if (LoadFromResource(assetName, out stream))
                        {
                            string path = Path.Combine(Path.GetTempPath(), Path.ChangeExtension(Path.GetTempFileName(), ".xnb"));

                            using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
                            {
                                byte[] buffer = new byte[stream.Length];

                                stream.Read(buffer, 0, buffer.Length);
                                fs.Write(buffer, 0, buffer.Length);
                            }

                            SceneManager.ContentManager.RootDirectory = Path.GetDirectoryName(path);
                            result = SceneManager.ContentManager.Load<T>(Path.GetFileNameWithoutExtension(path));
                        }
                    }

                    return result;
                }

                #endregion
            }

            return result;
        }

        public static T LoadFile<T>(string assetName) where T : class, IAsset<T>
        {
            T result = default(T);

            if (LoadFromFile(assetName, out result) && !cache.ContainsKey(assetName))
            {
                cache.Add(assetName, result);
            }

            return result;
        }

        public static string GetAssetName<T>(T obj) where T : class
        {
            return cache.FirstOrDefault(o => o.Value == obj).Key;
        }

        #region Privates

        private static bool LoadFromCache<T>(string assetName, out T asset) where T : class
        {
            if (cache.ContainsKey(assetName))
            {
                asset = cache[assetName] as T;
                return true;
            }

            asset = default(T);
            return false;
        }

        private static bool LoadFromResource<T>(string assetName, out T asset) where T : class
        {
            Stream stream;

            if (LoadFromResource(assetName, out stream))
            {
                asset = LoadAsset<T>(stream);
                return true;
            }

            asset = default(T);
            return false;
        }

        private static bool LoadFromResource(string assetName, out Stream stream)
        {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (!assembly.IsDynamic)
                {
                    string[] names = assembly.GetManifestResourceNames();

                    foreach (string name in names)
                    {
                        if (name.EndsWith(assetName))
                        {
                            stream = assembly.GetManifestResourceStream(name);
                            return true;
                        }
                    }
                }
            }

            stream = null;
            return false;
        }

        private static bool LoadFromFile<T>(string assetName, out T asset) where T : class
        {
            Stream stream;

            if (LoadFromFile(assetName, out stream))
            {
                using (stream)
                {
                    asset = LoadAsset<T>(stream);
                }

                return true;
            }

            asset = default(T);
            return false;
        }

        private static bool LoadFromFile(string assetName, out Stream stream)
        {
            string path = Path.Combine(GameInfo.ContentPath, assetName);

            if (File.Exists(path))
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                return true;
            }

            stream = null;
            return false;
        }

        private static T LoadAsset<T>(Stream stream) where T : class
        {
            IAsset<T> instance = (IAsset<T>)System.Activator.CreateInstance<T>();
            return instance.FromStream(stream);
        }

        #endregion

        #endregion
    }
}
