﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using DevEngine3.Systems;

namespace DevEngine3.Helpers
{
    public static class ExtrasHelper
    {
        #region Methods

        public static SerializableDictionary<string, object> GetExtras<T>(T obj)
        {
            SerializableDictionary<string, object> results = new SerializableDictionary<string, object>();

            foreach (PropertyInfo pInfo in Assets.Activator.GetPropertiesWithAttribute<SerializeExtraAttribute>(obj.GetType()))
            {
                results.Add(pInfo.Name, pInfo.GetValue(obj));
            }

            foreach (PropertyInfo pInfo in Assets.Activator.GetPropertiesWithAttribute<SerializeExtraFromAttribute>(obj.GetType()))
            {
                results.Add(pInfo.Name, pInfo.GetCustomAttribute<SerializeExtraFromAttribute>().GetValue(obj));
            }

            return results;
        }

        public static void SetExtras(object obj, SerializableDictionary<string, object> extras)
        {
            foreach (PropertyInfo pInfo in Assets.Activator.GetPropertiesWithAttribute<SerializeExtraAttribute>(obj.GetType()))
            {
                if (extras.ContainsKey(pInfo.Name))
                {
                    pInfo.SetValue(obj, extras[pInfo.Name]);
                }
            }

            foreach (PropertyInfo pInfo in Assets.Activator.GetPropertiesWithAttribute<SerializeExtraFromAttribute>(obj.GetType()))
            {
                if (extras.ContainsKey(pInfo.Name))
                {
                    pInfo.GetCustomAttribute<SerializeExtraFromAttribute>().SetValue(obj, extras[pInfo.Name]);
                }
            }
        }

        #endregion
    }
}
