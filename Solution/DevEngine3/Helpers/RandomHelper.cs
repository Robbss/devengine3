﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public static class RandomHelper
    {
        #region Fields

        private static Random random;

        #endregion

        #region Properties
        #endregion

        #region Constructors

        static RandomHelper()
        {
            random = new Random();
        }

        #endregion

        #region Methods

        public static string RandomString(string prefix = "")
        {
            return prefix + Guid.NewGuid().ToString();
        }

        public static float Between(float min, float max)
        {
            return min + (float)random.NextDouble() * (max - min);
        }

        public static Vector2 RandomDirection()
        {
            float angle = Between(0, MathHelper.TwoPi);
            return new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
        }

        public static float RandomFloat()
        {
            return (float)random.NextDouble();
        }

        #endregion
    }
}
