﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace DevEngine3
{
    public static class CollisionHelper
    {
        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Methods

        public static void CircleToCircle(Manifold m, RigidBody a, RigidBody b)
        {
            Circle A = a.shape as Circle;
            Circle B = b.shape as Circle;

            Vector2 normal = b.position - a.position;

            float dist_sqr = normal.LengthSquared();
            float radius = A.Radius + B.Radius;

            if (dist_sqr >= radius * radius)
            {
                m.contact_count = 0;
                return;
            }

            float distance = (float)Math.Sqrt(dist_sqr);

            m.contact_count = 1;

            if (distance == 0.0f)
            {
                m.penetration = A.Radius;
                m.normal = new Vector2(1, 0);
                m.contacts[0] = a.position;
            }
            else
            {
                m.penetration = radius - distance;
                m.normal = normal / distance;
                m.contacts[0] = m.normal * A.Radius + a.position;
            }
        }

        public static void CircleToPolygon(Manifold m, RigidBody a, RigidBody b)
        {
            Circle A = a.shape as Circle;
            Polygon B = b.shape as Polygon;

            m.contact_count = 0;

            Vector2 center = a.position;
            center = B.U.Transpose() * (center - b.position);

            float separation = -float.MaxValue;
            int faceNormal = 0;

            for (int i = 0; i < B.m_vertexCount; i++)
            {
                float s = Vector2.Dot(B.m_normals[i], center - B.m_vertices[i]);

                if (s > A.Radius)
                {
                    return;
                }

                if (s > separation)
                {
                    separation = s;
                    faceNormal = i;
                }
            }

            Vector2 v1 = B.m_vertices[faceNormal];
            int i2 = faceNormal + 1 < B.m_vertexCount ? faceNormal + 1 : 0;
            Vector2 v2 = B.m_vertices[i2];

            if (separation < Physics.Epsilon)
            {
                m.contact_count = 1;
                m.normal = -(B.U * B.m_normals[faceNormal]);
                m.contacts[0] = m.normal * A.Radius + a.position;
                m.penetration = A.Radius;

                return;
            }

            float dot1 = Vector2.Dot(center - v1, v2 - v1);
            float dot2 = Vector2.Dot(center - v2, v1 - v2);

            m.penetration = A.Radius - separation;

            if (dot1 <= 0.0f)
            {
                Vector2 c = center - v1;

                if (Vector2.Dot(c, c) > A.Radius * A.Radius)
                {
                    return;
                }

                m.contact_count = 1;

                Vector2 n = v1 - center;
                n = B.U * n;
                n.Normalize();

                m.normal = n;

                v1 = B.U * v1 + b.position;
                m.contacts[0] = v1;
            }
            else if (dot2 <= 0.0f)
            {
                Vector2 c = center - v2;

                if (Vector2.Dot(c, c) > A.Radius * A.Radius)
                {
                    return;
                }

                m.contact_count = 1;

                Vector2 n = v2 - center;
                v2 = B.U * v2 + b.position;

                m.contacts[0] = v2;

                n = B.U * n;
                n.Normalize();

                m.normal = n;
            }
            else
            {
                Vector2 n = B.m_normals[faceNormal];

                if (Vector2.Dot(center - v1, n) > A.Radius * A.Radius)
                {
                    return;
                }

                n = B.U * n;

                m.normal = -n;
                m.contacts[0] = m.normal * A.Radius + a.position;
                m.contact_count = 1;
            }
        }

        public static void PolygonToCircle(Manifold m, RigidBody a, RigidBody b)
        {
            CircleToPolygon(m, b, a);
            m.normal = -m.normal;
        }

        public static float FindAxisLeastPenetration(ref int faceIndex, Polygon A, Polygon B)
        {
            float bestDistance = -float.MaxValue;
            int bestIndex = 0;

            for (int i = 0; i < A.m_vertexCount; i++)
            {
                Vector2 n = A.m_normals[i];
                Vector2 nw = A.U * n;

                Matrix2 buT = B.U.Transpose();
                n = buT * nw;

                if (n != Vector2.Zero)
                {

                }

                Vector2 s = B.GetSupport(-n);

                Vector2 v = A.m_vertices[i];

                v = A.U * v + A.Body.position;
                v -= B.Body.position;
                v = buT * v;

                float d = Vector2.Dot(n, s - v);

                if (d > bestDistance)
                {
                    bestDistance = d;
                    bestIndex = i;
                }
            }

            faceIndex = bestIndex;
            return bestDistance;
        }

        public static void FindIncidentFace(Vector2[] v, Polygon RefPoly, Polygon IncPoly, int referenceIndex)
        {
            Vector2 referenceNormal = RefPoly.m_normals[referenceIndex];

            referenceNormal = RefPoly.U * referenceNormal;
            referenceNormal = IncPoly.U.Transpose() * referenceNormal;

            int incidentFace = 0;
            float minDot = float.MaxValue;

            for (int i = 0; i < IncPoly.m_vertexCount; i++)
            {
                float dot = Vector2.Dot(referenceNormal, IncPoly.m_normals[i]);

                if (dot < minDot)
                {
                    minDot = dot;
                    incidentFace = i;
                }
            }

            v[0] = IncPoly.U * IncPoly.m_vertices[incidentFace] + IncPoly.Body.position;
            incidentFace = incidentFace + 1 >= IncPoly.m_vertexCount ? 0 : incidentFace + 1;
            v[1] = IncPoly.U * IncPoly.m_vertices[incidentFace] + IncPoly.Body.position;
        }

        public static int Clip(Vector2 n, float c, ref Vector2[] face)
        {
            int sp = 0;
            Vector2[] ot = new Vector2[]
            {
                face[0],
                face[1]
            };

            float d1 = Vector2.Dot(n, face[0]) - c;
            float d2 = Vector2.Dot(n, face[1]) - c;

            if (d1 <= 0.0f)
            {
                ot[sp++] = face[0];
            }
            if (d2 <= 0.0f)
            {
                ot[sp++] = face[1];
            }

            if (d1 * d2 < 0.0f)
            {
                float alpha = d1 / (d1 - d2);
                ot[sp] = face[0] + alpha * (face[1] - face[0]);
                sp++;
            }

            face[0] = ot[0];
            face[1] = ot[1];

            if (sp != 3)
            {
                //throw new Exception();
            }

            return sp;
        }

        public static void PolygonToPolygon(Manifold m, RigidBody a, RigidBody b)
        {
            Polygon A = a.shape as Polygon;
            Polygon B = b.shape as Polygon;

            m.contact_count = 0;

            int faceA = 0;
            float penetrationA = FindAxisLeastPenetration(ref faceA, A, B);

            if (penetrationA >= 0.0f)
            {
                return;
            }

            int faceB = 0;
            float penetrationB = FindAxisLeastPenetration(ref faceB, B, A);

            if (penetrationB >= 0.0f)
            {
                return;
            }

            int referenceIndex = 0;
            bool flip = false;

            Polygon RefPoly;
            Polygon IncPoly;

            if (BiasGreaterThan(penetrationA, penetrationB))
            {
                RefPoly = A;
                IncPoly = B;
                referenceIndex = faceA;
                flip = false;
            }
            else
            {
                RefPoly = B;
                IncPoly = A;
                referenceIndex = faceB;
                flip = true;
            }

            Vector2[] incidentFace = new Vector2[2];
            FindIncidentFace(incidentFace, RefPoly, IncPoly, referenceIndex);

            Vector2 v1 = RefPoly.m_vertices[referenceIndex];
            referenceIndex = referenceIndex + 1 == RefPoly.m_vertexCount ? 0 : referenceIndex + 1;
            Vector2 v2 = RefPoly.m_vertices[referenceIndex];

            v1 = RefPoly.U * v1 + RefPoly.Body.position;
            v2 = RefPoly.U * v2 + RefPoly.Body.position;

            Vector2 sidePlaneNormal = (v2 - v1);
            sidePlaneNormal.Normalize();

            Vector2 refFaceNormal = new Vector2(sidePlaneNormal.Y, -sidePlaneNormal.X);

            float refC = Vector2.Dot(refFaceNormal, v1);
            float negSide = -Vector2.Dot(sidePlaneNormal, v1);
            float posSide = Vector2.Dot(sidePlaneNormal, v2);

            if (Clip(-sidePlaneNormal, negSide, ref incidentFace) < 2)
            {
                return;
            }

            if (Clip(sidePlaneNormal, posSide, ref incidentFace) < 2)
            {
                return;
            }

            m.normal = flip ? -refFaceNormal : refFaceNormal;

            int cp = 0;
            float separation = Vector2.Dot(refFaceNormal, incidentFace[0]) - refC;

            if (separation <= 0.0f)
            {
                m.contacts[cp] = incidentFace[0];
                m.penetration = -separation;
                ++cp;
            }
            else
            {
                m.penetration = 0;
            }

            separation = Vector2.Dot(refFaceNormal, incidentFace[1]) - refC;

            if (separation <= 0.0f)
            {
                m.contacts[cp] = incidentFace[1];
                m.penetration += -separation;
                ++cp;

                m.penetration /= (float)cp;
            }

            m.contact_count = cp;
        }

        private static bool BiasGreaterThan(float a, float b)
        {
            float k_biasRelative = 0.95f;
            float k_biasAbsoluet = 0.01f;
            return a >= b * k_biasRelative + a * k_biasAbsoluet;
        }


        #endregion
    }
}
