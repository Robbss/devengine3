﻿using System;
using System.Collections.Generic;
using DevEngine3;
using DevEngine3.Systems;
using DevEngine3.Interfaces;
using DevEngine3.Globals;
using DevEngine3.Containers;

namespace DevEngine3Test
{
    public static class Program
    {
        static Program()
        {
            AssemblyResolver.PrepareAssemblyResolver();
        }

        [STAThread]
        static void Main()
        {
            Multiplayer.StartServer<ServerForm>(25565, 60);

            //DGame.Play("GameInfo.xml");
        }
    }
}
