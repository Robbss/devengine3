﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using DevEngine3;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;
using DevEngine3.Systems;
using DevEngine3.Containers;

namespace DevEngine3Test
{
    public class MyEnt : Entity
    {
        #region Fields

        private bool follow = false;
        World world;

        #endregion

        #region Properties

        [SerializeExtra]
        public bool Follow
        {
            get
            {
                return follow;
            }
            set
            {
                follow = value;
            }
        }

        #endregion

        #region Constructors

        public MyEnt(string name) : base(name)
        {
        }

        public MyEnt(string name, Scene scene) : base(name, scene)
        {
        }

        #endregion

        #region Methods

        public override void Setup()
        {            
            Scene scene = new Scene("test-scene", new SceneModifiers(true));
            Entity entity = new MyEnt("test-entity", scene);

            entity.AddComponent<Renderer>().SetTexture("fisk.png");

            entity.AddComponent<Shader>().SetEffect("test.fx");
            entity.GetComponent<Shader>().Techniques.Add("LightingTechnique");

            Camera camera = new Camera("test-camera", scene);

            SceneContainer container = SceneContainer.GetContainer(scene);

            Assets.Xml.Write(container, "Content/TestScene.xml");
            

            /*
            Transform.Size = new Sizef(200, 200);

            world = new World();

            Polygon po = new Polygon();
            po.SetBox(50, 50);

            AddComponent<ParticleSystem>();

            RigidBody b = (RigidBody)AddComponent(new RigidBody(po));

            b.restitution = 1f;
            b.dynamicFriction = 0.4f;
            b.staticFriction = 0.3f;

            GetComponent<Transform>().Position += new Vector2(20, 0);
            b.angularVelocity += 0.5f;

            world.bodies.Add(b);

            Entity e = new Entity("test-2", Scene);

            e.AddComponent<Renderer>();

            Polygon poly = new Polygon();

            RigidBody bo = (RigidBody)e.AddComponent(new RigidBody(poly));
            bo.SetStatic();
            e.GetComponent<Transform>().Position = new Vector2(0, 0);
            e.GetComponent<Transform>().Size = new Sizef(500, 100);
            e.GetComponent<Renderer>().Invalidate();

            poly.SetBox(250, 50);

            world.bodies.Add(bo);
            */

            //b.orientation = 0.5f;
            //bo.orientation = 0.5f;

            //Scene.Camera.Transform.Position -= new Vector2(100, 100);

            GetComponent<Shader>().SetParameter("Opacity", 0f);

            base.Setup();
        }

        public override void Update(GameTime gameTime)
        {
            //world.Step();
            //Scene.Camera.CenterAt(Transform);
            base.Update(gameTime);

            Shader shader = GetComponent<Shader>();

            float current = shader.GetParameter<float>("Opacity");
            current += 0.1f * (float)gameTime.ElapsedGameTime.TotalSeconds;

            shader.SetParameter("Opacity", current);

            if (Input.GetKey(Keys.Right))
            {
                Camera.Current.Translate(100 * (float)gameTime.ElapsedGameTime.TotalSeconds, 0);
            }
            if (Input.GetKey(Keys.Left))
            {
                Camera.Current.Translate(-100 * (float)gameTime.ElapsedGameTime.TotalSeconds, 0);
            }

            if (Input.GetMouseButtonDown(MouseButtons.Left))
            {
                Transform.Center = Input.WorldMousePosition;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        #endregion
    }
}
