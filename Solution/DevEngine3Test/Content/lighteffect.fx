﻿sampler TextureSampler : register(s0);

float Opacity = 1;

struct PixelToFrame
{
	float4 Color : COLOR0;
};

struct VertexToPixel
{
	float4 Position     : POSITION;
	float4 Color        : COLOR0;
	float3 Position3D    : TEXCOORD0;
};

PixelToFrame PixelShaderFunction(VertexToPixel PSIn)
{
	PixelToFrame output = (PixelToFrame)0;

	output.Color = tex2D(TextureSampler, PSIn.Position3D.xy) * float4(1, 1, 1, Opacity);

	return output;
}

technique LightingTechnique
{
	pass Pass0
	{
		PixelShader = compile ps_4_0_level_9_1 PixelShaderFunction();
	}
}